<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" xmlns:math="http://exslt.org/math"
    version="3.0">

    <!--
        Copyright (c) 2015 Hannes Riebl
        Copyright (c) 2015–2019 Michelle Weidling
        Copyright (c) 2020–2021 Stefan Hynek
        Copyright (c) 2021 Simon Sendler

        This file is part of bdnPrint.

        bdnPrint is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        bdnPrint is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with bdnPrint.  If not, see <https://www.gnu.org/licenses/>.
    -->

    <xsl:output method="text"/>

    <xsl:strip-space elements="*"/>
    <xsl:preserve-space
        elements="abbr byline corr docImprint edition head hi
        item label lem note p pb persName rdg sic term titlePart titlePage"/>

    <!-- in case of the following elements we only run apply-templates:
        - TEI
        - front[ancestor::group]
        - text
        - div
        - docDate
        - docImprint
        - docTitle
        - byline
        - abbr
        - orig
        - lem
        - back
        - rs
        - l
        - lg
    Thus they have no template match of their own.
    -->


    <xsl:template match="teiHeader"/>

<!-- no idea what this is for...
    <xsl:template match="body">
        <xsl:if test="ancestor::group">
            <xsl:text>\setupnotation[footnote][numbercommand=\gobbleoneargument, rule=off]</xsl:text>
            <xsl:text>\setupnote[footnote][textcommand=\gobbleoneargument, rule=off]</xsl:text>
        </xsl:if>
        <xsl:apply-templates/>
    </xsl:template>
-->

    <!-- when edition text starts, page numbering in Arabic letters
        starts with 1. On the left page (even numbers) a short bibliographic
        info in the column title is displayed, on the right page (odd numbers)
        the current chapter -->
    <xsl:template match="group">
        <xsl:text>\startbodymatter </xsl:text>
        <xsl:text>\start\setups[Custompenalties]</xsl:text>
        <xsl:text>\noheaderandfooterlines </xsl:text>
        <xsl:text>\marking[evenHeader]{\tfx\it </xsl:text>
        <xsl:apply-templates select="//teiHeader//title[@type = 'column-title']"/>
        <xsl:text>}</xsl:text>

        <xsl:apply-templates/>
        <xsl:text>\stop\stopbodymatter\noheaderandfooterlines\startbackmatter </xsl:text>
        <xsl:text>\noheaderandfooterlines </xsl:text>
    </xsl:template>


    <xsl:template match="text[ancestor::group]">
        <!-- normally a new page is created after \stopbodymatter automatically, but
        since the column title isn't hideable with \noheaderandfooterlines this has been
        deactivated in header.tex and a new page without column title is created manually-->
        <xsl:if test="ancestor::group/descendant::text[1] = .">
            <xsl:text>\noheaderandfooterlines</xsl:text>
            <xsl:text>\setcounter[userpage][1]</xsl:text>
        </xsl:if>

        <xsl:apply-templates/>
    </xsl:template>


    <!-- within modern editorial text -->
    <xsl:template match="front[not(ancestor::group)]">
        <!-- frontmatter necessary for Roman page numbering (specified in header.tex) -->
        <xsl:text>\startfrontmatter </xsl:text>
        <xsl:apply-templates/>
        <xsl:text>\stopfrontmatter </xsl:text>
    </xsl:template>


    <xsl:template match="floatingText">
        <xsl:text>\startfloatingText </xsl:text>
        <xsl:apply-templates/>
        <xsl:text>\stopfloatingText </xsl:text>
    </xsl:template>


    <xsl:template match="div[@type = 'epilogue']">
        <xsl:text>\newPage </xsl:text>
        <xsl:apply-templates/>
    </xsl:template>


    <xsl:template match="div[@type = 'imprimatur']">
        <xsl:text>\newOddPage </xsl:text>
        <xsl:apply-templates/>
    </xsl:template>


    <xsl:template match="div[@type = 'privilege']">
        <xsl:text>\newOddPage </xsl:text>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="div[@type = 'entry']">
        <xsl:if test="not(preceding-sibling::*[1][self::head])">
            <xsl:text>\vspace[Quarterline] </xsl:text>
        </xsl:if>
        <xsl:apply-templates/>
    </xsl:template>

    <!-- outside edition text -->
    <xsl:template match="div[@type = 'preface' and ancestor::group]">
        <xsl:text>\newOddPage </xsl:text>

        <xsl:if test="ancestor::text[1]/descendant::div[@type = 'preface'][1] = .">
            <xsl:choose>
                <xsl:when test="descendant::supplied[@reason = 'toc-title']"/>
                <!--                    <xsl:apply-templates select="descendant::supplied[@reason ='toc-title']"/>-->
                <!--                </xsl:when>-->
                <xsl:otherwise>
                    <xsl:text>\marking[oddHeader]{Vorreden}</xsl:text>
                    <xsl:text>\writetolist[part]{}{Vorreden}</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>

        <xsl:apply-templates/>
    </xsl:template>


    <!-- within modern editorial text -->
    <xsl:template match="div[@type = 'preface' and not(ancestor::group)]">
        <!-- according to publisher guidelines the first page has to be 'V' when there
            is no dedication -->
        <xsl:text>\setcounter[userpage][5]</xsl:text>
        <xsl:call-template name="make-both-columns">
            <xsl:with-param name="contents" select="head[1]"/>
        </xsl:call-template>
        <xsl:apply-templates/>
        <!-- this is at the end of the TOC(???); pagebreaks are to be inserted in front only!
        <xsl:text>\newOddPage </xsl:text>
        -->
    </xsl:template>


    <xsl:template match="titlePage">
        <xsl:if test="not(ancestor::rdg or ancestor::group/descendant::titlePage[1] = .)">
            <xsl:text>\newOddPage </xsl:text>
        </xsl:if>
        <xsl:if test="ancestor::group/descendant::titlePage[1] = .">
            <xsl:text>\starteffect[hidden].\stopeffect</xsl:text>
            <xsl:text>\blank[36pt]</xsl:text>
        </xsl:if>
        <xsl:text>{\startalignment[center]</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>\stopalignment}</xsl:text>
    </xsl:template>


    <xsl:template match="ab[@type = 'half-title']">
        <xsl:if test="ancestor::body[1]/descendant::ab[@type = 'half-title'][1] = .">
            <xsl:text>\newOddPage </xsl:text>

            <xsl:if test="descendant::supplied">
                <xsl:apply-templates select="descendant::supplied"/>
            </xsl:if>

            <xsl:text>\starteffect[hidden].\stopeffect</xsl:text>

            <!-- since the interlinespace in rdgs is smaller we need less vertical space -->
            <xsl:choose>
                <xsl:when test="ancestor::rdg">
                    <xsl:text>\blank[50pt]</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>\blank[60pt]</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
        <xsl:if test="self::*[@rend = 'center-aligned']">
            <xsl:text>\startalignment[center]</xsl:text>
        </xsl:if>
        <xsl:apply-templates/>
        <xsl:if test="self::*[@rend = 'center-aligned']">
            <xsl:text>\stopalignment</xsl:text>
        </xsl:if>
    </xsl:template>


    <xsl:template match="titlePart">
        <xsl:apply-templates/>

        <xsl:if test="descendant::supplied">
            <xsl:apply-templates select="descendant::supplied"/>
        </xsl:if>
    </xsl:template>


    <xsl:template match="closer">
        <xsl:text>\blank\noindentation </xsl:text>
        <xsl:apply-templates/>
    </xsl:template>


    <xsl:template match="div[@type = 'index']">
        <xsl:text>\par\newOddPage{\switchtobodyfont[8.5pt] </xsl:text>
        <xsl:call-template name="make-both-columns">
            <xsl:with-param name="contents" select="head[1]"/>
        </xsl:call-template>
        <xsl:apply-templates/>
        <xsl:text>}</xsl:text>
    </xsl:template>


    <xsl:template match="div[@type = 'editorial-notes']">
        <xsl:text>{\switchtobodyfont[8.5pt] </xsl:text>
        <!-- hier einzeln -->
        <xsl:text>\marking[evenHeader]{</xsl:text>
        <xsl:value-of select="head[1]"/>
        <xsl:text>}</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}</xsl:text>
    </xsl:template>



    <!-- within critical text -->
    <xsl:template match="div[@type = 'contents' and not(@subtype = 'print')]">
        <xsl:text>\marking[oddHeader]{Inhalt}</xsl:text>
        <xsl:choose>
            <xsl:when test="ancestor::front/descendant::div[@type = 'contents'][1] = .">
                <xsl:text>\newOddPage </xsl:text>
            </xsl:when>
            <!-- only first toc has to be on an odd page -->
            <xsl:otherwise>
                <xsl:text>\newPage </xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:apply-templates/>
    </xsl:template>


    <!-- within modern editorial text -->
    <xsl:template match="div[@type = 'contents' and @subtype = 'print']">
        <xsl:apply-templates select="descendant::divGen[@type = 'contents']"/>
        <!-- this is at the end of the TOC; pagebreaks are to be inserted in front only!
        <xsl:text>\newOddPage </xsl:text>
        -->
    </xsl:template>


    <xsl:template match="div[@type = 'part']">
        <xsl:choose>
            <xsl:when
                test="
                    preceding-sibling::*[1][self::ab][@type = 'half-title']
                    or preceding-sibling::*[1][self::app[rdg/child::*[matches(., '[\w]')][last()][self::ab]]]"/>
            <xsl:otherwise>
                <xsl:text>\newOddPage </xsl:text>
            </xsl:otherwise>
        </xsl:choose>

        <xsl:apply-templates/>
    </xsl:template>


    <xsl:template match="div[@type = 'chapter']">
        <xsl:text>\newOddPage </xsl:text>
        <xsl:apply-templates/>
    </xsl:template>


    <!-- within critical text -->
    <xsl:template match="div[@type = 'introduction' and ancestor::group]">
        <xsl:if test="not(preceding-sibling::*[1][descendant::ab[@type = 'half-title']])">
            <xsl:text>\newOddPage </xsl:text>
        </xsl:if>
        <xsl:text>\noheaderandfooterlines </xsl:text>
        <xsl:apply-templates/>
    </xsl:template>


    <!-- within modern editorial text -->
    <xsl:template match="div[@type = 'introduction' and not(ancestor::group)]">
        <xsl:text>\newOddPage\resetcounter[footnote]</xsl:text>
        <xsl:call-template name="make-both-columns">
            <xsl:with-param name="contents" select="head[1]"/>
        </xsl:call-template>
        <xsl:apply-templates/>
    </xsl:template>


    <xsl:template match="div[@type = 'section']">
        <xsl:choose>
            <!-- b/s hat auch kein part/chapter; soll aber auch so behandelt werden-->
            <xsl:when test="ancestor::div[@type = ('part', 'chapter')]">
                <xsl:text>\startdivsection </xsl:text>
                <xsl:apply-templates/>
                <xsl:text>\stopdivsection </xsl:text>
            </xsl:when>
            <!-- for Leß' sermons -->
            <xsl:otherwise>
                <xsl:text>\newPage </xsl:text>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <xsl:template match="div[@type = 'corrigenda']">
        <xsl:text>\startcorrsection </xsl:text>
        <xsl:apply-templates/>
        <xsl:text>\stopcorrsection</xsl:text>
    </xsl:template>


    <xsl:template match="div[@type = 'editorial']">
        <xsl:text>\newOddPage\resetcounter[footnote]</xsl:text>
        <xsl:call-template name="make-both-columns">
            <xsl:with-param name="contents" select="head[1]"/>
        </xsl:call-template>
        <xsl:apply-templates/>
        <!-- this is at the end of the editorial; pagebreaks are to be inserted in front only!
        <xsl:text>\newOddPage </xsl:text>
        -->
    </xsl:template>

    <!-- dedications only appear in Bahrdt/Semler and Teller so far -->
    <xsl:template match="div[@type = 'dedication']">
        <xsl:text>\newPage </xsl:text>
        <xsl:apply-templates/>
    </xsl:template>

    <!-- special case Leß: paragraphs that are set like headings, but are not -->
    <xsl:template match="div[@type='single-div' and count(child::p) lt 3 and not(child::head)]">
        <xsl:text>\vspace[lineheight]</xsl:text>
        <xsl:text>\par \indentation</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>\vspace[lineheight]</xsl:text>
    </xsl:template>

    <xsl:template match="div[@type = 'synopsis' ]">
    <xsl:text>\startpostponingnotes </xsl:text>
        <xsl:apply-templates select="head"/>
        <xsl:text>\startxtable[Twocolumns]\startxrow </xsl:text>
        <xsl:for-each select="./div/head">
            <xsl:text>\startxcell </xsl:text>
            <xsl:apply-templates/>
            <xsl:text>\stopxcell </xsl:text>
        </xsl:for-each>
        <xsl:text>\stopxrow </xsl:text>
        <xsl:for-each select="./div[1]/ab">
            <xsl:text>\startxrow\startxcell </xsl:text>
            <xsl:apply-templates/>
            <xsl:text>\stopxcell\startxcell </xsl:text>
            <xsl:variable name="id" select="replace(@corresp, '#', '')"/>
            <xsl:apply-templates select="ancestor::div[@type = 'synopsis' ]//ab[@xml:id = $id]"/>
            <xsl:text>\stopxcell\stopxrow </xsl:text>
        </xsl:for-each>
        <xsl:text>\stopxtable\stoppostponingnotes\flushnotes </xsl:text>
    </xsl:template>

    <!-- for headings outside the edition text -->
    <xsl:template match="head[not(ancestor::group)]">
        <xsl:choose>
            <xsl:when
                test="
                    @type = 'sub'
                    or parent::div[not(@type) or @type = 'single-div']/preceding-sibling::*[1][self::head]">
                <xsl:text>\subtitle[]{</xsl:text>
                <xsl:apply-templates/>
                <xsl:text>}</xsl:text>
            </xsl:when>
            <xsl:when test="parent::div[@type and not(@type = 'single-div')]">
                <xsl:choose>
                    <xsl:when test="parent::div[@type = ('editorial', 'introduction')]">
                        <xsl:text>\maintitle[]{</xsl:text>
                        <xsl:apply-templates/>
                        <xsl:text>}</xsl:text>
                    </xsl:when>
                    <xsl:when test="parent::div[@type = 'editorial-notes']">
                        <xsl:text>\editorialtitle[]{</xsl:text>
                        <xsl:apply-templates/>
                        <xsl:text>}</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>\title{</xsl:text>
                        <xsl:apply-templates/>
                        <xsl:text>}</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>

                <xsl:text>\writetolist[part]{}{</xsl:text>
                <xsl:apply-templates/>
                <xsl:text>}</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>\notTOCsection[]{</xsl:text>
                <xsl:apply-templates/>
                <xsl:text>}</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <xsl:template match="head[ancestor::list and ancestor::group]">
        <xsl:choose>
            <xsl:when
                test="
                    ancestor::rdg and (@type = 'main' or not(@type))
                    and ancestor::list[1]/preceding-sibling::*[1][self::head]">
                <xsl:text>\listmainheadrdgfirst[]{</xsl:text>
            </xsl:when>
            <xsl:when test="ancestor::rdg and (@type = 'main' or not(@type))">
                <xsl:text>\listmainheadrdg[]{</xsl:text>
            </xsl:when>
            <xsl:when test="ancestor::rdg and @type = 'sub'">
                <xsl:text>\listsubheadrdg[]{</xsl:text>
            </xsl:when>
            <xsl:when
                test="
                    ancestor::lem and (@type = 'main' or not(@type))
                    and ancestor::div[@type = 'contents']/descendant::list[1] = ./parent::list">
                <xsl:text>\listfirsthead[]{</xsl:text>
            </xsl:when>
            <xsl:when test="ancestor::lem and (@type = 'main' or not(@type))">
                <xsl:text>\listmainhead[]{</xsl:text>
            </xsl:when>
            <xsl:when test="ancestor::lem and @type = 'sub'">
                <xsl:text>\listsubhead[]{</xsl:text>
            </xsl:when>
        </xsl:choose>
        <xsl:apply-templates/>
        <xsl:text>}</xsl:text>
    </xsl:template>


    <xsl:template match="head[not(ancestor::list) and ancestor::group]">
        <!--<xsl:if test="parent::div[@type = 'introduction' or @type = 'part' or @type = 'chapter']">-->

        <xsl:if test="descendant::supplied[@reason = 'toc-title']">
            <xsl:apply-templates select="descendant::supplied[@reason = 'toc-title']"/>
        </xsl:if>

        <xsl:if test="descendant::supplied[@reason = 'column-title']">
            <xsl:apply-templates select="descendant::supplied[@reason = 'column-title']"/>
        </xsl:if>

        <xsl:if test="parent::group">
            <xsl:call-template name="make-edition-title">
                <xsl:with-param name="element" select="."/>
            </xsl:call-template>
        </xsl:if>

        <xsl:if test="not(parent::group)">
            <xsl:choose>
                <xsl:when test="ancestor::rdg and self::head[@type='main']">
                    <!-- rdgsubject soll so groß sein wie subsubject; head selector @type=main -->
                    <!-- rdgsubsubject eine nummer kleiner; head selector @type=sub -->
                    <!-- selector für rdg ist ptl, ppl -->
                    <xsl:text>\rdgsubject[]{</xsl:text>
                </xsl:when>
                <xsl:when test="ancestor::rdg and self::head[@type='sub']">
                    <xsl:text>\rdgsubsubject[]{</xsl:text>
                </xsl:when>

                <xsl:when test="parent::div[@type = 'contents'] and following-sibling::list">
                    <xsl:if
                        test="
                            ancestor::text[1]/descendant::div[@type = 'contents'][1]
                            = parent::div[@type = 'contents']">
                        <xsl:call-template name="make-top-margin"/>
                    </xsl:if>
                    <xsl:text>\listhead[]{</xsl:text>
                </xsl:when>

                <xsl:when test="parent::div[@type = 'preface']">
                    <xsl:text>\prefacehead[]{</xsl:text>
                </xsl:when>

                <xsl:when test="parent::div[@type = 'corrigenda']">
                    <xsl:text>\corrigendaheading[]{</xsl:text>
                </xsl:when>

                <xsl:when
                    test="
                        ancestor::div[@type = 'chapter']/child::head[1] = .
                        or ancestor::div[@type = 'part']/descendant::head[1] = .
                        ">
                    <xsl:text>\chapterhead[]{</xsl:text>
                </xsl:when>

                <xsl:when test="count(child::*) = 1 and child::choice/orig[not(text())]">
                    <xsl:text>{</xsl:text>
                </xsl:when>

                <!-- headings of type main and sub in div type floatingText -->
                <xsl:when test="@type = 'main' and ancestor::floatingText">
                    <xsl:text>\floatingmaintitle[]{</xsl:text>
                </xsl:when>

                <xsl:when test="@type = 'sub' and ancestor::floatingText
                    or parent::div[@type = 'synopsis' ]">
                    <xsl:text>\floatingsubtitle[]{</xsl:text>
                </xsl:when>

                <!-- headings of type main and sub in div type section -->
                <xsl:when test="@type = 'main' and ancestor::div[@type = 'section']">
                    <xsl:text>\subject{</xsl:text>
                </xsl:when>

                <xsl:when test="@type = 'sub' and ancestor::div[@type = 'section']">
                    <xsl:text>\subsubject{</xsl:text>
                </xsl:when>


                <xsl:when test="@type = 'sub'">
                    <xsl:text>\editionsubtitle[]{</xsl:text>
                </xsl:when>

                <xsl:when test="@type = 'question'">
                    <xsl:text>\qsubject[]{</xsl:text>
                </xsl:when>

                <xsl:otherwise>
                    <xsl:text>\subject[]{</xsl:text>
                </xsl:otherwise>
            </xsl:choose>

            <xsl:if test="ancestor::floatingText">
                <xsl:text>\startfloatingHead </xsl:text>
            </xsl:if>

            <xsl:apply-templates/>

            <xsl:if test="ancestor::floatingText">
                <xsl:text>\stopfloatingHead </xsl:text>
            </xsl:if>
            <xsl:text>}</xsl:text>
        </xsl:if>
    </xsl:template>


    <xsl:template match="p[ancestor::group and not(@rend)]">
        <xsl:choose>
            <xsl:when
                test="
                    ancestor::div/descendant::p[1] = .
                    or parent::note/descendant::p[1] = .">
                <xsl:text>\noindentation </xsl:text>
            </xsl:when>
            <!-- first paragraphs of multi-paragraph items need no macro -->
            <xsl:when test="parent::item/descendant::p[1] = .">
            </xsl:when>
            <xsl:when
                test="
                    preceding-sibling::p
                    or not(ancestor::div/descendant::p[1] = .)">
                <xsl:if test="not(parent::rdg) or parent::rdg[ @type = 'ptl' or 'ppl' ]">
                    <xsl:if
                        test="
                            not(preceding-sibling::*[1][child::*[last()][self::list]]
                            or preceding-sibling::*[1][self::floatingText]
                            or preceding-sibling::*[1][child::*[last()][self::*[@rend = 'margin-horizontal']]]
                            or preceding-sibling::*[1][self::*[@rend = 'margin-horizontal']]
                            or preceding-sibling::*[1][self::app[rdg[@type = 'varying-structure']]][lem[descendant::*[last()][ancestor::list]]])">
                        <xsl:text>\par </xsl:text>
                    </xsl:if>
                </xsl:if>
            </xsl:when>
        </xsl:choose>
        <xsl:apply-templates/>
        <xsl:if test="@break-after = 'yes'">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template
        match="p[ancestor::front and not(ancestor::group or @rend or preceding-sibling::*[1][self::head])]">
        <xsl:text>\par </xsl:text>

        <xsl:apply-templates/>
        <xsl:if test="@break-after = 'yes'">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="p[parent::div[@type = 'index' or 'editorial-notes' and @subtype = 'print']]">
        <xsl:if test="parent::div/child::p[1] = .">
            <xsl:text>\noindentation </xsl:text>
        </xsl:if>
        <xsl:text>\par </xsl:text>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="p[@rend = 'margin-vertical']">
        <xsl:text>\blank[big]</xsl:text>
        <xsl:text>\noindentation </xsl:text>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="p[@rend = 'margin-horizontal']">
        <xsl:text>\startmarginHorizontal </xsl:text>
        <xsl:apply-templates/>
        <xsl:text>\stopmarginHorizontal </xsl:text>
    </xsl:template>

    <xsl:template match="opener[@rend = 'center-aligned']">
        <xsl:text>\startalignment[center]</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>\stopalignment</xsl:text>
    </xsl:template>


    <xsl:template match="p[@rend = 'right-aligned']">
        <xsl:text>\startalignment[flushright]</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>\stopalignment</xsl:text>
    </xsl:template>

    <xsl:template match="p[@rend = 'center-aligned']">
        <xsl:choose>
            <xsl:when
                test="
                    parent::*/child::*[1] = .
                    or descendant::lb">
                <xsl:text>\startalignment[center]</xsl:text>
                <xsl:apply-templates/>
                <xsl:text>\stopalignment</xsl:text>
            </xsl:when>
            <!-- for short center aligned paragraphs -->
            <xsl:otherwise>
                <xsl:text>\crlf</xsl:text>
                <xsl:text>{\midaligned{</xsl:text>
                <xsl:apply-templates/>
                <xsl:text>}}</xsl:text>
                <xsl:text>\crlf </xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="hi" mode="#all">
        <xsl:choose>
            <xsl:when test="not(@rend)">
                <xsl:text>\italic{</xsl:text>
                <xsl:apply-templates/>
                <xsl:text>}</xsl:text>

                <xsl:if test="@break-after-slanted">
                    <xsl:text>\hspace[postSlanted]</xsl:text>
                </xsl:if>
            </xsl:when>

            <!-- only used for describing the bold bracket in editorial introduction -->
            <xsl:when test="@rend = 'bold'">
                <xsl:text>\bb{}</xsl:text>
            </xsl:when>

            <xsl:when test="@rend = 'small-caps'">
                <xsl:text>{\sc </xsl:text>
                <xsl:apply-templates/>
                <xsl:text>}</xsl:text>
            </xsl:when>

            <xsl:when test="@rend = 'compact'">
                <!-- to be done -->
            </xsl:when>

            <xsl:when test="@rend = 'spaced-out'">
                <xsl:text>\Spaced{</xsl:text>
                    <xsl:variable name="words" select="tokenize(./text(), '@')"/>
                    <xsl:variable name="word-no" select="count($words)"/>
                    <xsl:choose>
                        <xsl:when test="$word-no gt 1">
                            <xsl:for-each select="$words">
                                <xsl:value-of select="."/>
                                <xsl:text>\spacedInterword</xsl:text>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates/>
                        </xsl:otherwise>
                    </xsl:choose>
                <xsl:text>} </xsl:text>
                <xsl:if test="starts-with(following::text()[1], '@')">
                    <xsl:text>\hspace[spacedInterword]</xsl:text>
                </xsl:if>
            </xsl:when>

            <xsl:when test="@rend = 'margin-vertical'">
                <xsl:text>\\</xsl:text>
                <xsl:apply-templates/>
            </xsl:when>

            <xsl:when test="@rend = 'margin-horizontal'">
                <xsl:text>\startmarginHorizontal </xsl:text>
                <xsl:apply-templates/>
                <xsl:text>\stopmarginHorizontal\noindentation </xsl:text>
            </xsl:when>

            <xsl:when test="@rend = 'subscript'">
                <xsl:text>{\tx\low{</xsl:text>
                <xsl:apply-templates/>
                <xsl:text>}}</xsl:text>
            </xsl:when>

            <xsl:when test="@rend = 'superscript'">
                <xsl:text>\high{{\tx </xsl:text>
                <xsl:apply-templates/>
                <xsl:text>}}</xsl:text>
            </xsl:when>

            <xsl:when test="@rend = 'overline'">
                <xsl:text>\overbar{</xsl:text>
                <xsl:apply-templates/>
                <xsl:text>}</xsl:text>
            </xsl:when>

            <xsl:when test="@rend = 'strike-through'">
                <xsl:text>\overstrike{</xsl:text>
                <xsl:apply-templates/>
                <xsl:text>}</xsl:text>
            </xsl:when>
        </xsl:choose>

        <xsl:if
            test="
                @break-after = 'yes'
                and not(following-sibling::*[1][self::pb])">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>


    <xsl:template match="aligned">
        <xsl:choose>
            <xsl:when test="@rend = 'right-aligned'">
                <xsl:choose>
                    <xsl:when test="ancestor::rdg[@type = ('pp', 'pt')]">
                        <xsl:apply-templates/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>\crlf </xsl:text>
                        <xsl:text>\rightaligned{</xsl:text>
                        <xsl:apply-templates/>
                        <xsl:text>}</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>

            <xsl:when test="@rend = 'center-aligned'">
                <xsl:choose>
                    <xsl:when test="child::lg">
                        <xsl:text>\startcenteredVerses </xsl:text>
                        <xsl:for-each select="lg/l">
                            <xsl:apply-templates select="."/>
                        </xsl:for-each>
                        <xsl:text>\stopcenteredVerses </xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>\startalignment[center]</xsl:text>
                        <xsl:apply-templates/>
                        <xsl:text>\stopalignment</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
        </xsl:choose>
    </xsl:template>


    <xsl:template match="choice" mode="#all">
        <xsl:apply-templates select="child::*[not(self::supplied or self::sic or self::expan)]"/>
        <xsl:if test="@break-after = 'yes'">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>


    <xsl:template match="expan"/>


    <xsl:template match="sic"/>





    <xsl:template match="app">
        <xsl:if test="not(lem[@type = 'missing-structure'])">
            <xsl:apply-templates/>
        </xsl:if>

        <xsl:choose>
            <xsl:when test="
            following::text()[1][matches(., '^[,\.;:\?!]')]">
                <xsl:text>\zerowidthnobreakspace </xsl:text>
            </xsl:when>
            <xsl:when test="@break-after = 'yes'">
                <xsl:text> </xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>


    <!-- ppl: paraphrase-lang=text ersetzt; ptl: parenthese-lang=text eingeschoben -->
   <xsl:template match="rdg[@type = ('ppl', 'ptl')]">
        <!-- second and third part: avoid vertical space when a head is followed directly by a rdg[@type = 'ppl' or @type = 'ptl'] -->
        <xsl:choose>
            <xsl:when
                test="
                    parent::app/parent::p/preceding-sibling::*[1][self::head]
                    and not(preceding-sibling::lem/*)
                    and not(parent::app/preceding-sibling::*)">
                <xsl:text>\startrdgbeforehead </xsl:text>
            </xsl:when>
            <xsl:when test="parent::app[ancestor::p][following-sibling::*]">
                <xsl:text>\startRdgInP </xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>\startrdg </xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="child::*[1][self::p]">
            <xsl:call-template name="paragraph-indent"/>
        </xsl:if>

        <xsl:apply-templates/>

        <xsl:if test="position() != last() and not(titlePage)">
            <xsl:text>\par </xsl:text>
        </xsl:if>
        <xsl:choose>
            <xsl:when
                test="
                    parent::app/parent::p/preceding-sibling::*[1][self::head]
                    and not(preceding-sibling::lem/*)">
                <xsl:text>\stoprdgbeforehead </xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>\stoprdg </xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <xsl:template match="rdg[@type = 'pt']">
        <xsl:if test="not(preceding-sibling::rdg[@type = ('pp', 'pt')])">
            <!-- when the first descendant node of a non-first p is a rdg[@type = 'pt'], we don't
                need this hspace since it would interfere with \hspace[p] (resulting in no
                whitespace at all)-->
            <xsl:if
                test="
                    not(descendant::text()[matches(., '[\w]')][1] =
                    ancestor::p[1]/descendant::text()[matches(., '[\w]')][1])
                    or ../preceding-sibling::*[1][self::rdgMarker[@type = ('om', 'ppl') and @mark = 'open']]">
                <xsl:text>\hspace[insert]</xsl:text>
            </xsl:if>
            <xsl:text>{\dvl}</xsl:text>
        </xsl:if>
        <xsl:call-template name="make-app-entry">
            <xsl:with-param name="node" select="."/>
            <xsl:with-param name="wit" select="replace(@wit, '[# ]+', '')"/>
        </xsl:call-template>
    </xsl:template>


    <xsl:template match="rdg[@type = 'pp']" mode="default">
        <xsl:if test="not(preceding-sibling::rdg[@type = ('pp', 'pt')])">
            <xsl:text>\hspace[insert]{\dvl}</xsl:text>
        </xsl:if>
        <xsl:call-template name="make-app-entry">
            <xsl:with-param name="node" select="."/>
            <xsl:with-param name="wit" select="replace(@wit, '[# ]+', '')"/>
        </xsl:call-template>
    </xsl:template>


    <xsl:template match="rdg[@type = 'v']" mode="default">
        <xsl:call-template name="make-app-entry">
            <xsl:with-param name="node" select="."/>
            <xsl:with-param name="wit" select="replace(@wit, '[# ]+', '')"/>
        </xsl:call-template>
    </xsl:template>


    <xsl:template
        match="rdg[@type = ('om', 'typo-correction', 'varying-structure', 'v', 'pp', 'varying-target')]"/>


    <xsl:template match="rdgMarker">
        <xsl:variable name="wit" select="replace(@wit, ' ', '')"/>
        <!-- see comment on alignment in the template for
            milestone[@type = 'separator'] -->
        <xsl:if test="following-sibling::*[1][self::milestone] and following-sibling::*[2][self::rdgMarker]">
            <xsl:text>\startalignment[middle] </xsl:text>
        </xsl:if>

        <xsl:choose>
            <xsl:when test="@type = 'v' and @mark = 'close' and parent::lem">
                <xsl:variable name="refs-array" select="tokenize(@ref, ' ')"/>

                <xsl:choose>
                    <xsl:when test="count($refs-array) = 1">
                        <xsl:variable name="ref" select="./@ref"/>
                        <xsl:apply-templates select="//rdg[@id = $ref]" mode="default"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="set-all-variants">
                            <xsl:with-param name="iii" select="1"/>
                            <xsl:with-param name="limit" select="count($refs-array)"/>
                            <xsl:with-param name="refs" select="$refs-array"/>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="@type = ('ppl', 'pp') and @context = 'lem'">
                <xsl:if test="@mark = 'open'">
                    <xsl:text>{\tfx\high{/</xsl:text>
                    <xsl:value-of select="$wit"/>
                    <xsl:text>}}</xsl:text>
                </xsl:if>
                <xsl:if test="@mark = 'close'">
                    <xsl:text>{\tfx\high{</xsl:text>
                    <xsl:value-of select="$wit"/>
                    <xsl:text>\textbackslash}}</xsl:text>

                    <xsl:if test="@type = 'pp'">
                        <xsl:variable name="ref" select="./@ref"/>
                        <xsl:variable name="refs-array" select="tokenize(@ref, ' ')"/>
                        <xsl:choose>
                            <xsl:when test="count($refs-array) = 1">
                                <xsl:apply-templates select="//rdg[@id = $ref]" mode="default"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:call-template name="set-all-variants">
                                    <xsl:with-param name="iii" select="1"/>
                                    <xsl:with-param name="limit" select="count($refs-array)"/>
                                    <xsl:with-param name="refs" select="$refs-array"/>
                                </xsl:call-template>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:if>
                </xsl:if>
            </xsl:when>
            <xsl:when test="@type = 'ptl'">
                <xsl:if test="@mark = 'open'">
                    <xsl:text>\margin{}{plOpen}{</xsl:text>
                    <xsl:value-of select="@ref"/>
                    <xsl:text>}{\tfx\high{</xsl:text>
                    <xsl:value-of select="$wit"/>
                    <xsl:text>}}{</xsl:text>
                    <xsl:value-of select="$wit"/>
                    <xsl:text>}</xsl:text>
                </xsl:if>
                <xsl:if test="@mark = 'close'">
                    <xsl:text>\margin{}{plClose}{</xsl:text>
                    <xsl:value-of select="@ref"/>
                    <xsl:text>}{\tfx\high{</xsl:text>
                    <xsl:value-of select="$wit"/>
                    <xsl:text>}}{</xsl:text>
                    <xsl:value-of select="$wit"/>
                    <xsl:text>}</xsl:text>
                </xsl:if>
            </xsl:when>
            <xsl:when test="@type = 'ppl' and @context = 'rdg'">
                <xsl:text>{\tfx\high{</xsl:text>
                <xsl:value-of select="$wit"/>
                <xsl:text>}}</xsl:text>
            </xsl:when>
            <xsl:when test="@type = 'om'">
                <xsl:if test="@mark = 'open'">
                    <xsl:text>\margin{}{omOpen}{</xsl:text>
                    <xsl:value-of select="@ref"/>
                    <xsl:text>}{\tfx\high{/</xsl:text>
                    <xsl:value-of select="$wit"/>
                    <xsl:text>}}{/</xsl:text>
                    <xsl:value-of select="$wit"/>
                    <xsl:text>}</xsl:text>
                </xsl:if>
                <xsl:if test="@mark = 'close'">
                    <xsl:text>\margin{}{omClose}{</xsl:text>
                    <xsl:value-of select="@ref"/>
                    <xsl:text>}{\tfx\high{</xsl:text>
                    <xsl:value-of select="$wit"/>
                    <xsl:text>\textbackslash}}{</xsl:text>
                    <xsl:value-of select="$wit"/>
                    <xsl:text>\textbackslash}</xsl:text>
                </xsl:if>
            </xsl:when>
        </xsl:choose>

        <!-- scribal abbreviations that don't have the same type and end at the same place should be
        seperated in order to avoid confusion.
        Exception: when an om and a ppl end at the same place, no extra whitespace is needed. they can
        easily distinguished by their backslashes. -->
        <xsl:if test="@mark = 'close' and @type = ('v', 'pp')">
            <xsl:text>~</xsl:text>
        </xsl:if>

        <xsl:if test="preceding-sibling::*[1][self::milestone] and preceding-sibling::*[2][self::rdgMarker]">
            <xsl:text>\stopalignment </xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- because of ascenders and descenders in Hebrew letters the font size has to
         be reduced in order to fit into the layout
    -->
    <xsl:template match="foreign[@xml:lang = 'hbo']">
        <xsl:text>\starthbo </xsl:text>
        <xsl:apply-templates/>
        <xsl:text>\stophbo </xsl:text>
        <xsl:if test="@break-after = 'yes'">
            <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="following::text()[1][starts-with(., '.')]">
                <xsl:text>\hspace[hebrpunct]</xsl:text>
            </xsl:when>
            <xsl:when test="following::text()[1][starts-with(., ')')]">
                <xsl:text>\hspace[hebrNegSpace]</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="foreign">
        <xsl:apply-templates/>
        <xsl:if test="@break-after = 'yes'">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="pb" mode="editorial-corrigenda">
        <xsl:text>\vl </xsl:text>
    </xsl:template>



    <xsl:template match="pb">
        <xsl:if
            test="
                @break-before = 'yes'
                and not(preceding-sibling::*[1][self::rdgMarker])">
            <xsl:text> </xsl:text>
        </xsl:if>

        <xsl:choose>
            <!-- pagebreaks in the critical apparatus have to be displayed as |a123| -->
            <xsl:when test="ancestor::rdg[@type = ('v', 'pp', 'pt')]">
                <xsl:text>{\vl}</xsl:text>
                <xsl:call-template name="make-pb-content">
                    <xsl:with-param name="pb" select="."/>
                </xsl:call-template>
                <xsl:text>{\vl}</xsl:text>
            </xsl:when>
            <!--problem, er weiß hier nicht, ob er im text oder in der tabelle am anfang ist-->
            <!--
            <xsl:when test="ancestor::corr[@type = ('editorial')]">
                <xsl:text>{\vl}</xsl:text>
            </xsl:when>
            -->
            <!--problem ende-->
            <xsl:otherwise>
                <xsl:call-template name="make-regular-pb">
                    <xsl:with-param name="current" select="."/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>

        <xsl:if
            test="
                @break-after = 'yes'
                and not(following-sibling::*[1][self::pb])">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>


    <!-- within critical text -->
    <xsl:template
        match="note[ancestor::group and not(@type = ('editorial-commentary', 'editorial-footnote'))]">
        <xsl:text>\startAuthorNote </xsl:text>
        <xsl:apply-templates/>
        <xsl:text>\stopAuthorNote\noindentation </xsl:text>
    </xsl:template>

    <!-- within modern editorial text -->
    <xsl:template match="note[not(ancestor::group or ancestor::editorial-notes)]">
        <xsl:text>\footnote{</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}</xsl:text>
    </xsl:template>


    <xsl:template match="note[@type = 'editorial-commentary']"/>

    <!-- note[@type = 'editorial-footnote'] is only relevant for Bahrdt/Semler where
    we have editorial footnotes in the edited text but no critical apparatus -->
    <xsl:template match="note[@type = 'editorial-footnote']">
        <xsl:text>\high{[}\editorialFootnote{</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}\high{]}</xsl:text>
    </xsl:template>


    <xsl:template match="bibl[@type = 'biblical-reference']">
        <xsl:choose>
            <xsl:when test="citedRange/@from">
                <xsl:variable name="from" select="tokenize(citedRange/@from, ':')"/>
                <xsl:variable name="from-bibl-book" select="$from[1]"/>
                <xsl:variable name="from-chapter" select="$from[2]"/>
                <xsl:variable name="from-verse" select="$from[3]"/>
                <xsl:variable name="from-passage" select="concat($from-chapter, ',', $from-verse)"/>
                <xsl:variable name="to" select="tokenize(citedRange/@to, ':')"/>
                <xsl:variable name="to-bibl-book" select="$to[1]"/>
                <xsl:variable name="to-chapter" select="$to[2]"/>
                <xsl:variable name="to-verse" select="$to[3]"/>
                <xsl:variable name="to-passage" select="concat($to-chapter, ',', $to-verse)"/>

                <xsl:text>\bibleIndex{</xsl:text>
                <xsl:value-of select="$from-bibl-book"/>
                <xsl:text>+</xsl:text>
                <xsl:choose>
                    <xsl:when test="$from-verse">
                        <xsl:value-of select="$from-passage"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$from-chapter"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:choose>
                    <xsl:when test="matches($to-bibl-book, '^f')">
                        <xsl:value-of select="$to-bibl-book"/>
                        <xsl:text>.</xsl:text>
                    </xsl:when>
                    <xsl:when
                        test="
                            $from-bibl-book = $to-bibl-book
                            and $from-chapter = $to-chapter">
                        <xsl:text>\endash </xsl:text>
                        <xsl:value-of select="$to-verse"/>
                    </xsl:when>
                    <xsl:when test="$from-bibl-book = $to-bibl-book">
                        <xsl:text>\endash </xsl:text>
                        <xsl:choose>
                            <xsl:when test="$to-verse">
                                <xsl:value-of select="$to-passage"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="$to-chapter"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <!-- does the other case ever occur? -->
                    <xsl:otherwise/>
                </xsl:choose>
                <xsl:text>}</xsl:text>
            </xsl:when>
            <xsl:when test="citedRange/@n">
                <xsl:variable name="reference" select="tokenize(citedRange/@n, ' ')"/>
                <xsl:for-each select="$reference">
                    <xsl:variable name="n" select="tokenize(., ':')"/>
                    <xsl:variable name="bibl-book" select="$n[1]"/>
                    <xsl:variable name="chapter" select="$n[2]"/>
                    <xsl:variable name="verse" select="$n[3]"/>
                    <xsl:variable name="passage" select="concat($chapter, ',', $verse)"/>

                    <xsl:text>\bibleIndex{</xsl:text>
                    <xsl:value-of select="$bibl-book"/>
                    <xsl:text>+</xsl:text>
                    <xsl:choose>
                        <xsl:when test="not($verse)">
                            <xsl:value-of select="$chapter"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$passage"/>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:text>}</xsl:text>
                </xsl:for-each>
            </xsl:when>
        </xsl:choose>
        <xsl:apply-templates/>
        <xsl:if test="@break-after = 'yes'">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="index">
        <xsl:for-each select="./term">
            <xsl:choose>
                <xsl:when test="@type = 'alternative' ">
                    <xsl:text>\see</xsl:text>
                    <xsl:value-of select="substring-before(parent::index/@indexName, '-')"/>
                    <xsl:text>Index{</xsl:text>
                    <xsl:value-of select="."/>
                    <xsl:text>}{</xsl:text>
                    <xsl:for-each select="parent::index/term[not(@type = 'alternative')]">
                        <xsl:choose>
                            <xsl:when test="./persName">
                                <xsl:value-of select="./persName"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="."/>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:if test="following-sibling::term[not(@type = 'alternative')]">
                            <xsl:text> u. </xsl:text>
                        </xsl:if>
                    </xsl:for-each>
                    <xsl:text>}</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>\</xsl:text>
                    <xsl:value-of select="substring-before(parent::index/@indexName, '-')"/>
                    <xsl:text>Index{</xsl:text>
                    <xsl:choose>
                        <xsl:when test="./persName">
                            <xsl:value-of select="./persName"/>
                            <xsl:text>+</xsl:text>
                            <xsl:for-each select="./*[not(self::persName)]">
                                <xsl:value-of select="."/>
                                <xsl:text> </xsl:text>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="."/>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:text>}</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="term"/>

    <!-- for the overview of editions in the modern introduction -->
    <xsl:template match="list">
        <xsl:choose>
            <xsl:when test="ancestor::div[@type = 'editorial']">
                <xsl:text>\starttwocolumns </xsl:text>
                <xsl:apply-templates/>
                <xsl:text>\stoptwocolumns </xsl:text>
                <xsl:text>\noindentation </xsl:text>
            </xsl:when>
            <!-- regular lists in base text are set as descriptions, which is a
                 more robust process, and Context-lists are derived from descriptions
                 anyway -->
            <xsl:otherwise>
                <xsl:text>\startEnumeration</xsl:text>
                <xsl:if test="ancestor::list">
                    <xsl:text>Sub</xsl:text>
                </xsl:if>
                <xsl:text> </xsl:text>
                <xsl:apply-templates/>
                <xsl:text>\stopEnumeration</xsl:text>
                <xsl:if test="ancestor::list">
                    <xsl:text>Sub</xsl:text>
                </xsl:if>
                <xsl:text> </xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="item">
        <xsl:choose>
            <xsl:when test="ancestor::div[@type = 'editorial'] and preceding-sibling::*[1][self::label]">
                <xsl:text>\NC </xsl:text>
                <xsl:apply-templates/>
                <xsl:text>\NC \NR </xsl:text>
            </xsl:when>
            <xsl:when test="ancestor::div[@type = 'contents']">
                <xsl:text>\sym{}</xsl:text>
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <!-- different macro b/c labels w/ app need larger block indentation -->
                    <xsl:when test="child::label[descendant::rdg[not(@type = 'typo-correction')]]">
                        <xsl:text>\startListLikeApp{</xsl:text>
                    </xsl:when>
                    <xsl:when test="child::*[1][self::rdgMarker]">
                        <xsl:text>\startListLikePpl{</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>\startListLike{</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
                <!-- if item is at the beginning of a paranthesis, the rdgMarker needs to be set first, cf. ll. 1334–1336 -->
                <xsl:if test=". = parent::list/item[1]">
                    <xsl:apply-templates select="rdgMarker[@type = 'ppl'][1]"/>
                </xsl:if>
                <xsl:apply-templates select="child::label"/>
                <xsl:text>}</xsl:text>
                <xsl:choose>
                    <xsl:when test=". = parent::list/item[1]">
                        <xsl:apply-templates select="child::node()[not(self::label)][not(self::rdgMarker[@type = 'ppl'][1])]"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="child::node()[not(self::label)]"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:choose>
                    <xsl:when test="child::label[descendant::rdg[not(@type = 'typo-correction')]]">
                        <xsl:text>\stopListLikeApp </xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>\stopListLike </xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="label">
        <xsl:choose>
            <xsl:when test=".[@type = 'headword' ]">
                <xsl:text>\Headword{</xsl:text>
                    <xsl:apply-templates/>
                <xsl:text>} </xsl:text>
            </xsl:when>
            <!-- this conflicts with #notelabel if label is not the direct ancestor of note -->
            <xsl:when test="ancestor::list and ancestor::div[@type = 'editorial']">
                <xsl:text>\NC </xsl:text>
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="@break-after = 'yes'">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>


    <xsl:template match="lb">
        <xsl:if test="not(preceding-sibling::*[1][self::head])">
            <xsl:text>\crlf </xsl:text>
        </xsl:if>
    </xsl:template>


    <xsl:template match="milestone[@type = 'structure']">
        <xsl:choose>
            <xsl:when
                test="
                    ancestor::rdg[@type = ('ppl', 'ptl')]
                    and preceding-sibling::node() and not(preceding-sibling::*[1][self::pb]
                    or preceding-sibling::*[1][self::rdgMarker]
                    or preceding-sibling::*[1][child::*[last()][self::list]])">
                <xsl:text>\crlf </xsl:text>
                <xsl:choose>
                    <xsl:when
                        test="
                            @unit = 'p' and (not(preceding-sibling::*[1][self::list])
                            and not(preceding-sibling::*[1][self::seg][child::*[last()][self::list]]))">
                        <xsl:call-template name="paragraph-indent"/>
                    </xsl:when>
                    <xsl:when
                        test="
                            @unit = 'p' and (preceding-sibling::*[1][self::list]
                            or preceding-sibling::*[1][self::seg][child::*[last()][self::list]])">
                        <xsl:text>\hspace[p] </xsl:text>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>
            <xsl:when
                test="
                    ancestor::rdg[@type = ('ppl', 'ptl')]
                    and preceding-sibling::node() and @unit = 'p' and (preceding-sibling::*[1][self::list]
                    or preceding-sibling::*[1][self::seg][child::*[last()][self::list]])">
                <xsl:call-template name="paragraph-indent"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="make-milestone">
                    <xsl:with-param name="milestone" select="."/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <xsl:template match="milestone[@unit = 'fn-break']">
        <xsl:choose>
            <!-- pagebreaks in the critical apparatus have to be displayed as |a123| -->
            <xsl:when test="ancestor::rdg[@type = ('v', 'pp', 'pt')]">
                <xsl:text>{\vl}</xsl:text>
                <xsl:call-template name="make-pb-content">
                    <xsl:with-param name="pb" select="."/>
                </xsl:call-template>
                <xsl:text>{\vl}</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="make-regular-pb">
                    <xsl:with-param name="current" select="."/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>

        <xsl:if
            test="
                @break-after = 'yes'
                and not(following-sibling::*[1][self::milestone[@unit]]
                or matches(substring(following::text()[1], 1, 1), '[a-z]'))">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="milestone[@type = 'separator']">
        <xsl:text>\Separator</xsl:text>
        <xsl:choose>
            <xsl:when test="./@rend">
                <xsl:value-of select="./@rend"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>star</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <!-- In case of preceding and following rdgMarkers, a macro without
                centering has to be used. As the alignment-environment needs to
                encompass the rdgMarkers as well, it is implemented when matching
                qualified rdgMarkers. -->
        <xsl:if test="not(preceding-sibling::*[1][self::rdgMarker] and following-sibling::*[1][self::rdgMarker])">
            <xsl:text>Centered</xsl:text>
        </xsl:if>
        <xsl:text> </xsl:text>
    </xsl:template>

    <xsl:template match="persName">
        <xsl:apply-templates/>
        <xsl:if test="@break-after = 'yes'">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>


    <xsl:template match="ptr[@type = 'editorial-commentary']">
        <xsl:choose>
            <xsl:when test="ancestor::rdg[@type = ('v', 'pp', 'pt')]">
                <!--<xsl:text>[E]</xsl:text>-->
                <xsl:text>\pagereference[</xsl:text>
                <xsl:value-of select="generate-id()"/>
                <xsl:text>]</xsl:text>
                <xsl:if test="@break-after = 'yes'">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>
                <!--<xsl:text>\margin{</xsl:text>
                <xsl:value-of select="generate-id()"/>
                <xsl:text>}{e}{}{\hbox{}}{E}</xsl:text>-->
                <xsl:text>\pagereference[</xsl:text>
                <xsl:value-of select="generate-id()"/>
                <xsl:text>]</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="ptr[@type = 'page-ref']">
        <xsl:variable name="target" select="replace(@target, '^.*?#', '')"/>
        <xsl:text>E\at[</xsl:text>
        <xsl:value-of select="$target"/>
        <xsl:text>]</xsl:text>
    </xsl:template>

    <xsl:template match="ptr">
        <xsl:if
            test="
                not(following::rdgMarker[@mark = 'close'])
                or @break-after = 'yes'">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="ref">
        <xsl:apply-templates/>
        <xsl:if test="@break-after = 'yes'">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>


    <xsl:template match="table">
        <xsl:text>\crlf \startxtable[table] </xsl:text>
        <xsl:text>\startxtablehead \startxrow[tablehead] </xsl:text>
        <xsl:choose>
            <xsl:when test="row[1]/cell[@role = 'label']">
                <xsl:for-each select="row[1]/cell[@role = 'label']">
                    <xsl:apply-templates select="."/>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>\startxcell[nx=</xsl:text>
                <xsl:value-of select="count(row[1]/cell)"/>
                <xsl:text>] \stopxcell</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>\stopxrow \stopxtablehead </xsl:text>
        <xsl:apply-templates/>
        <xsl:text>\stopxtable </xsl:text>
    </xsl:template>


    <xsl:template match="row">
        <xsl:if test="not(child::cell/@role = 'label')">
            <xsl:text>\startxrow</xsl:text>
            <xsl:if test="not(following-sibling::row)">
                <xsl:text>[bottomrow]</xsl:text>
            </xsl:if>
            <xsl:text> </xsl:text>
            <xsl:apply-templates/>
            <xsl:text>\stopxrow </xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- obsolete? -->
    <!--<xsl:template match="row[@rend = 'line']">
        <xsl:text>\LL </xsl:text>
    </xsl:template>-->


    <xsl:template match="cell">
        <xsl:text>\startxcell{</xsl:text>
        <xsl:if test="@rend = 'center-aligned'">
            <xsl:text>\midaligned{</xsl:text>
        </xsl:if>
        <xsl:apply-templates/>
        <xsl:if test="@rend = 'center-aligned'">
            <xsl:text>}</xsl:text>
        </xsl:if>
        <xsl:text>}\stopxcell </xsl:text>
    </xsl:template>


    <xsl:template match="seg">
        <xsl:apply-templates/>
        <xsl:if test="@break-after">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>


    <xsl:template match="seg[@type = 'row']">
        <!-- @TODO adjust -->
        <xsl:apply-templates/>
        <xsl:text>\NC \NR </xsl:text>
    </xsl:template>


    <xsl:template match="seg[@type = 'item']">
        <xsl:apply-templates/>
        <xsl:text>\crlf </xsl:text>
    </xsl:template>


    <xsl:template match="seg[@type = 'margin']">
        <xsl:text>\{</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>\} </xsl:text>
    </xsl:template>


    <xsl:template match="supplied[@reason = 'toc-title']">
        <xsl:choose>
            <xsl:when test="not(ancestor::titlePart[@type = 'main'])">
                <xsl:text>\writetolist[part]{}{</xsl:text>
                <xsl:apply-templates/>
                <xsl:text>}</xsl:text>
            </xsl:when>
            <xsl:when test="ancestor::titlePart[@type = 'main']">
                <xsl:call-template name="make-edition-title">
                    <xsl:with-param name="element" select="."/>
                </xsl:call-template>
            </xsl:when>
        </xsl:choose>
    </xsl:template>


    <xsl:template match="supplied[@reason = 'column-title']">
        <xsl:text>\marking[oddHeader]{</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}</xsl:text>
    </xsl:template>


    <xsl:template match="signed[not(ancestor::group)]">
        <xsl:text>\wordright{\italic{</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}}</xsl:text>
    </xsl:template>

    <xsl:template match="signed[ancestor::group]">
        <xsl:choose>
            <xsl:when test="child::lb">
                <xsl:text>\startalignment[left]</xsl:text>
                <xsl:apply-templates/>
                <xsl:text>\stopalignment</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>\wordright{</xsl:text>
                <xsl:apply-templates/>
                <xsl:text>}</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="supplied">
        <xsl:text>{[</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>]}</xsl:text>

        <!-- nbsp -->
        <xsl:if test="parent::hi[parent::lem/child::*[last()] = .]/child::*[last()] = .">
            <xsl:text>~</xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="divGen[@type = 'contents']">
        <xsl:call-template name="make-both-columns">
            <xsl:with-param name="contents" select="string('Inhalt')"/>
        </xsl:call-template>

        <xsl:text>\title[</xsl:text>
        <xsl:value-of select="preceding-sibling::head"/>
        <xsl:text>]{</xsl:text>
        <xsl:value-of select="preceding-sibling::head"/>
        <xsl:text>}</xsl:text>
        <xsl:text>\placecontent </xsl:text>
    </xsl:template>

    <xsl:template match="divGen[@type = 'editorial-notes']">
        <xsl:text>\blank[medium]</xsl:text>
        <xsl:for-each select="//ptr[@type = 'editorial-commentary']">
            <xsl:apply-templates
                select="./preceding::supplied[@reason = 'column-title'][1]"/>
            <xsl:text>\startannotation{</xsl:text>
            <xsl:variable name="target" select="replace(@target, '^#', '')"/>
            <xsl:text>\at[</xsl:text>
            <xsl:value-of select="generate-id()"/>
            <xsl:text>]}\pagereference[</xsl:text>
            <xsl:value-of select="$target"/>
            <xsl:text>]\italic{</xsl:text>
            <xsl:apply-templates select="//note[@xml:id = $target]/label"/>
            <xsl:text>}\crlf </xsl:text>
            <xsl:apply-templates select="//note[@xml:id = $target]/p"/>
            <xsl:text>\stopannotation</xsl:text>
        </xsl:for-each>

        <!-- missing \startpart
        <xsl:text>\stoppart </xsl:text>
        -->
        <!-- pagebreaks are to be inserted in front only!
        <xsl:text>\newOddPage </xsl:text>
        -->
    </xsl:template>


    <xsl:template match="divGen[@type = 'editorial-corrigenda']">
        <xsl:variable name="base-text" select="//witness[@n = 'base-text']/@xml:id"/>

        <xsl:choose>
            <xsl:when test="not(//witness[@n = 'base-text'])">
                <xsl:for-each select="//group/text[descendant::corr[@type = 'editorial']]">
                    <xsl:text>\subject[]{</xsl:text>
                    <!-- hier titel einfügen -->
                    <xsl:value-of select="(./descendant::supplied[@reason = 'column-title'])[1]"/>
                    <xsl:text>}</xsl:text>
                    <xsl:call-template name="make-editorial-corrigenda-no-base-text">
                        <xsl:with-param name="part" select="."/>
                    </xsl:call-template>
                </xsl:for-each>
            </xsl:when>

            <!-- in case we have several parts (teilbaende) we want to display the editorial corrigenda per part -->
            <xsl:when test="count(//group/text) gt 1">
                <xsl:for-each select="//group/text[descendant::corr[@type = 'editorial']]">
                    <xsl:text>\subject[]{Zu Teilband </xsl:text>
                    <xsl:value-of select="count(./preceding-sibling::text) + 1"/>
                    <xsl:text>}</xsl:text>
                    <xsl:call-template name="make-editorial-corrigenda">
                        <xsl:with-param name="base-text" select="$base-text"/>
                        <xsl:with-param name="part" select="."/>
                    </xsl:call-template>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="make-editorial-corrigenda">
                    <xsl:with-param name="base-text" select="$base-text"/>
                    <xsl:with-param name="part" select="//group/text"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <xsl:template
        match="divGen[@type = ('bible-index', 'persons-index', 'classics-index', 'subjects-index')]">
        <xsl:call-template name="make-indices">
            <xsl:with-param name="divGen" select="."/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="lg">
        <xsl:choose>
            <xsl:when test="parent::aligned[@rend = 'center-aligned']">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>{\startverses </xsl:text>
                <xsl:apply-templates/>
                <xsl:text>\stopverses}</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="l">
        <xsl:apply-templates/>
        <xsl:text>\crlf </xsl:text>
    </xsl:template>

    <xsl:template match="docAuthor">
        <xsl:apply-templates/>
        <xsl:if test="@break-after = 'yes'">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>


    <!-- creates a heading -->
    <xsl:template name="make-subject">
        <xsl:param name="content"/>

        <xsl:text>\subject[]{</xsl:text>
        <xsl:apply-templates select="$content"/>
        <xsl:text>}</xsl:text>
    </xsl:template>


    <xsl:template name="make-app-entry">
        <xsl:param name="wit"/>
        <xsl:param name="node"/>

        <xsl:text>\</xsl:text>
        <xsl:value-of select="$wit"/>
        <xsl:text>Note{</xsl:text>
        <xsl:if test="$node/ancestor::label[@type = 'headword']">
            <xsl:text>\bf </xsl:text>
        </xsl:if>

        <!--@DEPRECATED-->
        <!-- in some cases, a margin note starts with a rdg[@type = 'v'] which then
        should have a '{' indicating we're in a margin note. -->
        <!-- I have no idea, what this was supposed to do;
             there is no word about the requirement stated above in "Kritische Anlage der Ausgabe"
        <xsl:variable name="bibl" select="ancestor::bibl[@type = 'biblical-reference']"/>
        <xsl:variable name="rdg" select="."/>
        <xsl:if
            test="
                $bibl[parent::seg[@type = 'margin']/descendant::app[1] = $rdg/parent::app]
                and $bibl[parent::seg[@type = 'margin']/child::node()[1] = $rdg/parent::app]">
            <xsl:text>\{</xsl:text>
        </xsl:if>
        -->
        <xsl:apply-templates/>
        <xsl:text>}</xsl:text>

        <xsl:if
            test="
                following::node()[1][self::text()]
                or not($node/@type = $node/preceding-sibling::rdg/@type)">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>


    <xsl:template name="make-indices">
        <xsl:param name="divGen"/>
        <xsl:variable name="type" select="$divGen/@type"/>
        <xsl:variable name="index">
            <xsl:choose>
                <xsl:when test="$type = 'bible-index'">
                    <xsl:text>Bibelstellen</xsl:text>
                </xsl:when>
                <xsl:when test="$type = 'classics-index'">
                    <xsl:text>Antike Autoren</xsl:text>
                </xsl:when>
                <xsl:when test="$type = 'persons-index'">
                    <xsl:text>Personen</xsl:text>
                </xsl:when>
                <xsl:when test="$type = 'subjects-index'">
                    <xsl:text>Sachen</xsl:text>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>

        <xsl:text>\writetolist[chapter]{}{</xsl:text>
            <xsl:value-of select="$index"/>
        <xsl:text>}</xsl:text>
        <xsl:text>\marking[oddHeader]{</xsl:text>
            <xsl:value-of select="$index"/>
        <xsl:text>}</xsl:text>
        <xsl:text>\startsetups[a]</xsl:text>
        <xsl:text>\switchtobodyfont[10pt]</xsl:text>
        <xsl:text>\rlap{}</xsl:text>
        <xsl:text>\hfill</xsl:text>
        <xsl:text>{\tfx\it </xsl:text>
            <xsl:value-of select="$index"/>
        <xsl:text>}</xsl:text>
        <xsl:text>\hfill</xsl:text>
        <xsl:text> \llap{\pagenumber}</xsl:text>
        <xsl:text>\stopsetups</xsl:text>
        <xsl:text>\startcolumns</xsl:text>
        <xsl:text>\place</xsl:text>
        <xsl:value-of select="replace($type, '-i', 'I')"/>
        <xsl:text>\stopcolumns</xsl:text>
    </xsl:template>


    <xsl:template name="make-milestone">
        <xsl:param name="milestone"/>
        <xsl:variable name="edition" select="replace(@edRef, '[#\s]+', '')"/>

        <!-- defined in header.tex -->
        <xsl:if test="$milestone/@unit = 'p'">
            <xsl:text> \MilestoneP{}</xsl:text>
        </xsl:if>
        <xsl:if test="$milestone/@unit = 'line'">
            <xsl:text> \MilestoneLine{}</xsl:text>
        </xsl:if>
        <xsl:if test="$milestone/@unit = 'no-p'">
            <xsl:text> \MilestoneNoP{}</xsl:text>
        </xsl:if>
        <xsl:if test="$milestone/@unit = 'no-line'">
            <xsl:text> \MilestoneNoLine{}</xsl:text>
        </xsl:if>

        <xsl:text>{\tfx\high{</xsl:text>
        <xsl:value-of select="$edition"/>
        <xsl:text>}} </xsl:text>
    </xsl:template>


    <xsl:template name="make-regular-pb">
        <xsl:param name="current"/>

        <xsl:text>\margin{}{pb}{}{</xsl:text>
        <xsl:choose>
            <!-- when several pb occur on the same spot only the first one produces
                    a scribal abbreviation -->
            <xsl:when
                test="
                    ($current/preceding-sibling::node()[1][self::pb]
                    or not($current/preceding-sibling::node()[1][matches(., '\w')])
                    and $current/preceding-sibling::node()[2][self::pb])
                    or ($current/preceding-sibling::node()[1][self::milestone[@unit]]
                    or not($current/preceding-sibling::node()[1][matches(., '\w')])
                    and $current/preceding-sibling::node()[2][self::milestone[@unit]])">
                <xsl:text>\hbox{}</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>\vl</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>}{</xsl:text>
        <xsl:call-template name="make-pb-content">
            <xsl:with-param name="pb" select="$current"/>
        </xsl:call-template>
        <xsl:text>}</xsl:text>
    </xsl:template>


    <xsl:template name="make-pb-content">
        <xsl:param name="pb"/>

        <xsl:value-of select="replace($pb/@edRef, '[# ]+', '')"/>
        <xsl:if test="$pb/@type = 'sp'">
            <xsl:text>[</xsl:text>
        </xsl:if>
        <xsl:value-of select="$pb/@n"/>
        <xsl:if test="$pb/@type = 'sp'">
            <xsl:text>]</xsl:text>
        </xsl:if>
    </xsl:template>


    <xsl:template name="paragraph-indent">
        <xsl:text>\indenting </xsl:text>
        <!--
        <xsl:text>\starteffect[hidden]</xsl:text>
        <xsl:text>.</xsl:text>
        <xsl:text>\stopeffect</xsl:text>
        <xsl:text>\hspace[p]</xsl:text>
        -->
    </xsl:template>


    <xsl:template name="make-both-columns">
        <xsl:param name="contents"/>

        <xsl:text>\marking[oddHeader]{</xsl:text>
        <xsl:value-of select="$contents"/>
        <xsl:text>}</xsl:text>

        <xsl:text>\marking[evenHeader]{</xsl:text>
        <xsl:value-of select="$contents"/>
        <xsl:text>}</xsl:text>
    </xsl:template>


    <xsl:template name="find-prev-pbs">
        <xsl:param name="iii"/>
        <xsl:param name="limit"/>
        <xsl:param name="context"/>
        <xsl:param name="wits"/>
        <xsl:if test="$iii &lt;= $limit">
            <xsl:if test="$iii &gt; 1">
                <xsl:text>, </xsl:text>
            </xsl:if>
            <xsl:variable name="prev-pb"
                select="$context/preceding::pb[matches(@edRef, $wits[$iii])][1]"/>
            <xsl:value-of select="$wits[$iii]"/>
            <xsl:if test="$prev-pb/@type = 'sp'">
                <xsl:text>[</xsl:text>
            </xsl:if>
            <xsl:value-of select="$prev-pb/@n"/>
            <xsl:if test="$prev-pb/@type = 'sp'">
                <xsl:text>]</xsl:text>
            </xsl:if>
            <xsl:call-template name="find-prev-pbs">
                <xsl:with-param name="iii" select="$iii + 1"/>
                <xsl:with-param name="limit" select="$limit"/>
                <xsl:with-param name="context" select="$context"/>
                <xsl:with-param name="wits" select="$wits"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>


    <xsl:template name="set-all-variants">
        <xsl:param name="iii"/>
        <xsl:param name="limit"/>
        <xsl:param name="refs"/>

        <xsl:if test="$iii &lt;= $limit">
            <xsl:variable name="ref" select="$refs[$iii]"/>
            <xsl:apply-templates select="//rdg[@id = $ref]" mode="default"/>

            <xsl:call-template name="set-all-variants">
                <xsl:with-param name="iii" select="$iii + 1"/>
                <xsl:with-param name="limit" select="$limit"/>
                <xsl:with-param name="refs" select="$refs"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <!-- TODO: to be removed -->
    <xsl:template name="make-top-margin">
        <!-- publisher guidelines: new chapters should have a top margin of 36pt.
                a hidden character has to be inserted, otherwise ConTeXt won't display the
                empty space -->
        <!--<xsl:text>\starteffect[hidden] . \stopeffect\blank[36pt]</xsl:text>
        <xsl:text>\noindentation </xsl:text>-->
    </xsl:template>


    <xsl:template name="select-page-marker">
        <xsl:param name="corr"/>
        <xsl:param name="milestone"/>
        <xsl:param name="pb"/>

        <xsl:variable name="corr-bottom-note" select="$corr/ancestor::note[@place = 'bottom']"/>
        <xsl:variable name="ms-bottom-note" select="$milestone/ancestor::note[@place = 'bottom']"/>

        <xsl:choose>
            <xsl:when test="$corr-bottom-note = $ms-bottom-note">
                <xsl:call-template name="make-pb-content">
                    <xsl:with-param name="pb" select="$milestone"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="make-pb-content">
                    <xsl:with-param name="pb" select="$pb"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="make-editorial-corrigenda">
        <xsl:param name="base-text"/>
        <xsl:param name="part"/>

        <xsl:text>\startxtable[table] \startxtablehead \startxrow[tablehead]</xsl:text>
        <xsl:text>\startxcell[width=2.5cm] Seite \stopxcell</xsl:text>
        <xsl:text>\startxcell fehlerhaftes Original \stopxcell</xsl:text>
        <xsl:text>\startxcell stillschweigende Korrektur \stopxcell </xsl:text>
        <xsl:text>\stopxrow \stopxtablehead</xsl:text>
        <xsl:for-each select="$part//choice[child::corr[@type = 'editorial']][not( ancestor::ref[@type = 'note'] )]">
            <xsl:text>\startxrow</xsl:text>
            <xsl:text>\startxcell </xsl:text>
            <xsl:choose>
                <!-- Case: change of guidance text in reading. In this case
                    lem[@wit] is always descendant of a rdg. Corresponding page
                    numbers of all relevant wits are given.
                    This is a case given for corrigenda whose choice-elements
                    are direct descendants of the lem -->
                <xsl:when test="ancestor::*[1][self::lem[@wit]]">
                    <xsl:variable name="wit" select="string(ancestor::lem[1]/@wit)"/>
                    <xsl:variable name="prev-pb"
                        select="preceding::pb[matches(@edRef, $wit)][1]"/>
                    <xsl:call-template name="make-pb-content">
                        <xsl:with-param name="pb" select="$prev-pb"/>
                    </xsl:call-template>
                </xsl:when>
                <!-- Case: correction in a note not in a reading. Within notes,
                    page breaks are marked by milestone-elements! -->
                <xsl:when test="ancestor::note[@place = 'bottom'][not(ancestor::rdg)]">
                    <xsl:choose>
                        <xsl:when
                            test="ancestor::app[1]/lem[not(@wit)] = ./ancestor::lem[not(@wit)]">
                            <xsl:variable name="prev-fn-break"
                                select="preceding::milestone[@unit = 'fn-break'][matches(@edRef, $base-text)][1]"/>
                            <xsl:variable name="prev-pb"
                                select="ancestor::note[@place = 'bottom']/preceding::pb[matches(@edRef, $base-text)][1]"/>

                            <xsl:call-template name="select-page-marker">
                                <xsl:with-param name="corr" select="."/>
                                <xsl:with-param name="milestone" select="$prev-fn-break"/>
                                <xsl:with-param name="pb" select="$prev-pb"/>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when
                            test="
                                ancestor::app[1]/rdg = ./ancestor::rdg
                                or ancestor::app[1]/lem[@wit] = ./ancestor::lem[@wit]">
                            <xsl:variable name="wit" select="parent::rdg/@wit"/>
                            <xsl:variable name="bottom-note"
                                select="ancestor::note[@type = 'bottom']"/>
                            <xsl:variable name="prev-fn-break"
                                select="preceding::milestone[@unit = 'fn-break'][matches(@edRef, $wit)][1]"/>
                            <xsl:variable name="prev-pb"
                                select="ancestor::note[@place = 'bottom']/preceding::pb[matches(@edRef, $wit)][1]"/>

                            <xsl:call-template name="select-page-marker">
                                <xsl:with-param name="corr" select="."/>
                                <xsl:with-param name="milestone" select="$prev-fn-break"/>
                                <xsl:with-param name="pb" select="$prev-pb"/>
                            </xsl:call-template>
                        </xsl:when>
                    </xsl:choose>
                </xsl:when>
                <!-- Case: correction in a note within a reading. Corresponding
                    page numbers of all relevant wits -->
                <xsl:when test="ancestor::note[ancestor::rdg]">
                    <xsl:variable name="wit" select="string-join(ancestor::note/ancestor::rdg[1]/@wit)"/>
                    <xsl:variable name="prev-fn-break"
                        select="preceding::milestone[@unit = 'fn-break'][contains($wit, @edRef)][1]"/>
                    <xsl:variable name="prev-pb"
                        select="ancestor::note/ancestor::app[1]/preceding::pb[contains($wit, @edRef)][1]"/>

                    <xsl:choose>
                        <xsl:when test="$prev-fn-break">
                            <xsl:if test="$prev-fn-break/preceding-sibling::milestone[@unit = 'fn-break']">
                                <xsl:call-template name="make-pb-content">
                                    <xsl:with-param name="pb" select="$prev-fn-break/preceding-sibling::milestone[@unit = 'fn-break'][1]"/>
                                </xsl:call-template>
                                <xsl:text>, </xsl:text>
                            </xsl:if>
                            <xsl:call-template name="make-pb-content">
                                <xsl:with-param name="pb" select="$prev-fn-break"/>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="make-pb-content">
                                <xsl:with-param name="pb" select="$prev-pb"/>
                            </xsl:call-template>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <!-- Case: change of guidance text in reading. In this case
                    lem[@wit] is always descendant of a rdg. Corresponding page
                    numbers of all relevant wits are given -->
                <xsl:when test="ancestor::lem[@wit]">
                    <xsl:variable name="wit" select="string(ancestor::lem/@wit)"/>
                    <xsl:variable name="prev-pb"
                        select="preceding::pb[matches(@edRef, $wit)][1]"/>
                    <xsl:call-template name="make-pb-content">
                        <xsl:with-param name="pb" select="$prev-pb"/>
                    </xsl:call-template>
                </xsl:when>
                <!-- Case: Correction in reading. The corresponding page numbers
                    of all relevant wits in rdg/@wit are given -->
                <xsl:when test="ancestor::rdg">
                    <xsl:variable name="wit" select="ancestor::rdg[1]/@wit"/>
                    <xsl:choose>
                        <xsl:when test="matches($wit, ' #')">
                            <xsl:variable name="wits" select="replace($wit, '#', '')"/>
                            <xsl:variable name="wits-array" select="tokenize($wits, '\s')"/>
                            <xsl:call-template name="find-prev-pbs">
                                <xsl:with-param name="iii" select="1"/>
                                <xsl:with-param name="limit" select="count($wits-array)"/>
                                <xsl:with-param name="context" select="."/>
                                <xsl:with-param name="wits" select="$wits-array"/>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:variable name="prev-pb"
                                select="preceding::pb[matches(@edRef, $wit)][1]"/>
                            <xsl:variable name="prev-app-pb"
                                select="ancestor::app[1]/preceding::pb[matches(@edRef, $wit)][1]"/>

                            <xsl:choose>
                            <!-- second condition needed because preceding axis above matches choice//pb -->
                                <xsl:when test="$prev-pb and not(./sic/pb)">
                                    <xsl:call-template name="make-pb-content">
                                        <xsl:with-param name="pb" select="$prev-pb"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:call-template name="make-pb-content">
                                        <xsl:with-param name="pb" select="$prev-app-pb"/>
                                    </xsl:call-template>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <!-- Case: Correction in base text but not in wit(s). The
                    corresponding page number of all wits not in rdg/@wit are
                    given. Further ancestor::app are checked in case of nested
                    apparatus -->
                <xsl:when test="ancestor::lem">
                    <xsl:variable name="no-wits">
                        <xsl:for-each select="ancestor::app">
                            <xsl:value-of select="replace(string-join(./rdg/@wit), '#', '')"/>
                        </xsl:for-each>
                    </xsl:variable>
                    <xsl:variable name="no-wits-regex" select="string-join(('[', $no-wits, ']'), '')"/>
                    <!-- exclusion of witness z in the following variable is a
                        workaround for Less and might need to be changed/excluded
                        in other editions. z only appears in readings in Less -->
                    <xsl:variable name="all-wits" select="ancestor::TEI/teiHeader//witness/@xml:id"/>
                    <xsl:variable name="yes-wits">
                        <xsl:for-each select="$all-wits">
                            <xsl:if test="not(matches(., $no-wits-regex))">
                                <xsl:value-of select="."/>
                                <xsl:text> </xsl:text>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:variable>
                    <xsl:variable name="wits-array" select="tokenize(normalize-space($yes-wits), '\s')"/>
                    <xsl:call-template name="find-prev-pbs">
                        <xsl:with-param name="iii" select="1"/>
                        <xsl:with-param name="limit" select="count($wits-array)"/>
                        <xsl:with-param name="context" select="."/>
                        <xsl:with-param name="wits" select="$wits-array"/>
                    </xsl:call-template>
                </xsl:when>
                <!-- Base case: a correction not in any app element is present in
                    all wits. The corresponding page of the base text is given
                    first, all others follow -->
                <xsl:when test="not(ancestor::app)">
                    <xsl:variable name="prev-pb"
                        select="preceding::pb[matches(@edRef, $base-text)][1]"/>
                    <xsl:call-template name="make-pb-content">
                        <xsl:with-param name="pb" select="$prev-pb"/>
                    </xsl:call-template>
                    <xsl:for-each select="//witness[not(@n = 'base-text')]/@xml:id">
                        <xsl:text>, </xsl:text>
                        <xsl:variable name="prev-pb"
                            select="preceding::pb[matches(@edRef, .)][1]"/>
                        <xsl:call-template name="make-pb-content">
                            <xsl:with-param name="pb" select="$prev-pb"/>
                        </xsl:call-template>
                    </xsl:for-each>
                </xsl:when>
            </xsl:choose>
            <xsl:text> \stopxcell\startxcell </xsl:text>
            <xsl:apply-templates select="sic" mode="editorial-corrigenda"/>
            <xsl:text>\stopxcell\startxcell </xsl:text>
            <xsl:apply-templates select="child::corr" mode="editorial-corrigenda"/>
            <xsl:text>\stopxcell\stopxrow </xsl:text>
        </xsl:for-each>
        <xsl:text>\startxrow[bottomrow-edcorr]\startxcell[nx=3]\stopxcell\stopxrow\stopxtable </xsl:text>
    </xsl:template>

    <!-- for Bahrdt/Semler which has no base text -->
    <!-- do we need to keep this for non-B/S texts? -->
    <xsl:template name="make-editorial-corrigenda-no-base-text">
        <xsl:param name="part"/>
        <!-- Table Header -->
        <xsl:text>\starttabulatehead[]</xsl:text>
        <xsl:text>\FL </xsl:text>
        <xsl:text>\NC Seite </xsl:text>
        <xsl:text>\NC fehlerhaftes Original </xsl:text>
        <xsl:text>\NC stillschweigende Korrektur \NC \AR </xsl:text>
        <xsl:text>\LL </xsl:text>
        <xsl:text>\stoptabulatehead</xsl:text>
        <!-- Table Body -->
        <xsl:text>\starttabulate[|p(1cm)|p|p|] </xsl:text>
        <xsl:for-each select="$part//choice[child::corr[@type = 'editorial']]">
            <xsl:text> \NC </xsl:text>

            <xsl:call-template name="make-pb-content">
                <xsl:with-param name="pb" select="preceding::pb[1]"/>
            </xsl:call-template>

            <xsl:text> \NC </xsl:text>
            <xsl:apply-templates select="sic" mode="editorial-corrigenda"/>
            <xsl:text>\NC </xsl:text>
            <xsl:apply-templates select="child::corr" mode="editorial-corrigenda"/>
            <!--
            <xsl:for-each select="child::corr/node()[not(name()='pb')]">
                <xsl:apply-templates/>
            </xsl:for-each>
            -->
            <xsl:text>\NC \NR </xsl:text>
        </xsl:for-each>
        <xsl:text>\HL </xsl:text>
        <xsl:text>\stoptabulate </xsl:text>
    </xsl:template>

    <xsl:template match="corr" mode="editorial-corrigenda">
        <xsl:choose>
            <xsl:when test="ancestor::label[@type = 'headword' ]">
                <xsl:text>\bf </xsl:text>
            </xsl:when>
            <xsl:when test="./foreign[@xml:lang = 'hbo']">
                <xsl:text>[align=flushright]</xsl:text>
            </xsl:when>
        </xsl:choose>
        <xsl:text> </xsl:text>
        <xsl:apply-templates select="node()[not(name()='pb')]"/>
    </xsl:template>

    <xsl:template match="sic" mode="editorial-corrigenda">
        <xsl:choose>
            <xsl:when test="ancestor::label[@type = 'headword' ]">
                <xsl:text>\bf </xsl:text>
            </xsl:when>
            <xsl:when test="./foreign[@xml:lang = 'hbo']">
                <xsl:text>[align=flushright]</xsl:text>
            </xsl:when>
        </xsl:choose>
        <xsl:text> </xsl:text>
        <xsl:apply-templates select="node()[not(name()='pb')]"/>
    </xsl:template>

    <xsl:template name="make-edition-title">
        <xsl:param name="element"/>

        <xsl:text>\writebetweenlist[part]{</xsl:text>
        <xsl:text>{\startalignment[center]</xsl:text>
        <xsl:text>\subject[</xsl:text>
        <xsl:apply-templates select="$element/node()"/>
        <xsl:text>]{</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}\stopalignment}}</xsl:text>
    </xsl:template>

    <xsl:template match="pc">
        <xsl:apply-templates/>
        <xsl:if test="not(matches(./text(), '[(]'))">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>
