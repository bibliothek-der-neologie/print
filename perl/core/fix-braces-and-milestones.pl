#!/usr/bin/env perl

=begin comment
Copyright (c) 2015 Hannes Riebl
Copyright (c) 2015–2019 Michelle Weidling
Copyright (c) 2020 Stefan Hynek

This file is part of bdnPrint.

bdnPrint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

bdnPrint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bdnPrint.  If not, see <https://www.gnu.org/licenses/>.
=cut

# This script makes running text compliant to TeX directives.
# Oftentimes characters like '{' or '<' occur in the edition text or the
# editorial guidelines which are interpreted as TeX or LUA commands. Thus, they
# have to be escaped.


use autodie;
use strict;
use utf8;
use warnings;

use File::Slurp;

use Encode qw(encode decode);

my $head = "";

my $tail = decode('UTF-8', read_file("input/" . $ARGV[0] . ".xml"));


# handling curly braces (otherwise they are interpreted as TeX command)
$tail =~ s/\{/\\\{/g;
$tail =~ s/\}/\\\}/g;
$tail =~ s/([a-z])\\</$1\\textbackslash </g;
$tail =~ s/\|/\\|/g;
$tail =~ s/\%/\\%/g;

# handling of milestones
# e2 88 ab: UTF-8 integral
# e2 88 ac: UTF-8 double integral
$tail =~ s/\xe2\x88\xab/\\line\{\}/g;
$tail =~ s/\xe2\x88\xac/\\p\{\}/g;

$head = $head . $tail;
print encode('UTF-8', $head);

