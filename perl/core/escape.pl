#!/usr/bin/env perl

=begin comment
Copyright (c) 2015 Hannes Riebl
Copyright (c) 2015–2019 Michelle Weidling
Copyright (c) 2020 Stefan Hynek

This file is part of bdnPrint.

bdnPrint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

bdnPrint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bdnPrint.  If not, see <https://www.gnu.org/licenses/>.
=cut

# This script makes running text compliant to TeX directives.
# Oftentimes characters like '{' or '<' occur in the edition text or the
# editorial guidelines which are interpreted as TeX or LUA commands. Thus, they
# have to be escaped.

# characters that have to be escaped:
#    & % $ # _ { } ~ ^ \


use autodie;
use strict;
use utf8;
use warnings;

use File::Slurp;

use Encode qw(encode decode);

my $head = "";

my $tail = decode('UTF-8', read_file("tmp/" . $ARGV[0] . "_tmp-1.tex"));

my @escapists = ("#");

foreach my $char ( @escapists ) {
    $tail =~ s/$char/\\$char/g;
}

$tail =~ s/∫/\\MilestoneLine{}/g; # ∫
$tail =~ s/∬/\\MilestoneP{}/g; # ∬
$tail =~ s/ᴉ/\\Upsidedowni{}/g; # ᴉ


$head = $head . $tail;
print encode('UTF-8', $head);

