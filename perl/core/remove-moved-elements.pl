#!/usr/bin/env perl

=begin comment
Copyright (c) 2015 Hannes Riebl
Copyright (c) 2015–2019 Michelle Weidling
Copyright (c) 2020 Stefan Hynek

This file is part of bdnPrint.

bdnPrint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

bdnPrint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bdnPrint.  If not, see <https://www.gnu.org/licenses/>.
=cut

# This scipts removes any temporary changes made in postprocess-margins.pl.

use autodie;
use strict;
use utf8;
use warnings;

use File::Slurp;

open(FILE, "<tmp/moved_elements.txt") or die "$!\n";
my @notes = <FILE>;
close(FILE);

my $tmp = read_file("tmp/" . $ARGV[0] . "_tmp-4.tex");

for (my $i = 0; $i < @notes; $i++) {
  my $note = $notes[$i];
  chomp($note);

	my @elements = split(', ', $note);
	my $element = $elements[-1];
	my ($context) = $note =~ /(.*) $element/;
	# has to be escaped because of regex
	$note =~ s/([\/\\])/\\$1/g;

	my $pattern = $note . "\}";
	$tmp =~ s/$pattern/$context\}/g;
}

# replace !...! with [...]
$tmp =~ s/!([0-9XIV]*?)!/\[$1\]/g;

print $tmp;
