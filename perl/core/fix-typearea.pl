#!/usr/bin/env perl

=begin comment
Copyright (c) 2015 Hannes Riebl
Copyright (c) 2015–2019 Michelle Weidling
Copyright (c) 2020 Stefan Hynek

This file is part of bdnPrint.

bdnPrint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

bdnPrint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bdnPrint.  If not, see <https://www.gnu.org/licenses/>.
=cut

# DEPRECATED

use autodie;
use strict;
use utf8;
use warnings;

use File::Slurp;

my $head = "";

my $tail = read_file("tmp/" . $ARGV[0] . "_tmp-1.tex");

#$tail =~ s/\x{E2}/--/g;

#$tail =~ s/\x{E2}/--/g;


# fixing type area for Noesselt
$tail =~ s/mathematischen \{\\tfx\\high\{\/c}}Wissenschaften/mathematischen \{\\tfx\\high\{\/c}}Wissen- schaften/g;
$tail =~ s/\{\\tfx\\high\{\/c}}Wissenschaften,\\aNote\{Wissenschaften}/\{\\tfx\\high\{\/c}}Wissen- schaften,\\aNote\{Wissenschaften}/g;
$tail =~ s/uterungsschriften kan/u- terungsschriften kan/g;
$tail =~ s/\{\\tfx\\high\{\/c}}\\sachIndex\[KIRCHENGESCHICHTE\]\{Kirchengeschichte}Kirchengeschichte,/\{\\tfx\\high\{\/c}}\\sachIndex\[KIRCHENGESCHICHTE\]\{Kirchengeschichte}Kirchen- geschichte,/g;
$tail =~ s/\{d1e108956}\{\\tfx\\high\{\/c}}\{\/c}derselben/\{d1e108956}\{\\tfx\\high\{\/c}}\{\/c}dersel- ben/g;
$tail =~ s/genommne\\cNote\{genommene}/genomm- ne\\cNote\{genommene}/g;
$tail =~ s/im praktischen Verstan/im praktischen Ver-stan/g;
$tail =~ s/\{a246\}genth/\{a246\}gen-th/g;
$tail =~ s/348\/47/ 348\/47/g;
$tail =~ s/1580\) galt/ 1580\) galt/g;
$tail =~ s/1791\) in kaum/ 1791\) in kaum/g;
$tail =~ s/1801\) erschien/ 1801\) erschien/g;
$tail =~ s/ (1801\) erschien 1802)/$1/g;
$tail =~ s/1701\) ist 1698/ 1701\) ist 1698/g;
$tail =~ s/1803\) umfasst/ 1803\) umfasst/g;
$tail =~ s/1791\) verbinden sich/ 1791\) verbinden sich/g;


#doesn't work properly
$tail =~ s/\{\/c\\textbackslash\}derselben/\{\/c\\textbackslash\}dersel-ben/g;


$head = $head . $tail;
print $head;