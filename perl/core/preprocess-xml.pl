#!/usr/bin/env perl

=begin comment
Copyright (c) 2015 Hannes Riebl
Copyright (c) 2015–2019 Michelle Weidling
Copyright (c) 2020 Stefan Hynek

This file is part of bdnPrint.

bdnPrint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

bdnPrint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bdnPrint.  If not, see <https://www.gnu.org/licenses/>.
=cut

# Before transforming XML to TeX we first have to prune surplus white spaces
# and handle special cases (documented below) in the encoding.

use autodie;
use strict;
use utf8;
use warnings;

use File::Slurp;

use Encode qw(encode decode);

my $tmp = decode('UTF-8', read_file("tmp/" . $ARGV[0] . "_tmp.xml"));

# remove newlines, tabs and returns
$tmp =~ s/[\n\t\r]//g;
# remove spaces between ">" and "<"
$tmp =~ s/>\s+</></g;
# strip leftover multispaces to single space
$tmp =~ s/\s\s+/ /g;
# replace "@" with single whitespace
$tmp =~ s/@/ /g;

# fix placeholders
# @DEPRECATED
# $tmp =~ s/###//g;

# double hyphens ('--') have to be escaped because tex will render them '–'
# but be careful with xml comments!
$tmp =~ s/([^!])--([^>])/$1-\\\/-$2/g;


print encode('UTF-8', $tmp);
