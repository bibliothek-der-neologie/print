#!/usr/bin/env perl

=begin comment
Copyright (c) 2015 Hannes Riebl
Copyright (c) 2015–2019 Michelle Weidling
Copyright (c) 2020 Stefan Hynek

This file is part of bdnPrint.

bdnPrint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

bdnPrint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bdnPrint.  If not, see <https://www.gnu.org/licenses/>.
=cut

# Due to processes in the intermediate format and converting the data to TeX
# there are lots of surplus white spaces. This script removes them.

use autodie;
use strict;
use utf8;
use warnings;

use File::Slurp;

use Encode qw(encode decode);

my $head = "";

my $tail = decode('UTF-8', read_file("tmp/" . $ARGV[0] . "_tmp-1.tex"));

$tail =~ s/\s+/ /g;
$tail =~ s/ +/ /g;
$tail =~ s/ ~/~/g;
$tail =~ s/~ /~/g;

# remove all whitespaces at the beginning and end of TeX expressions
$tail =~ s/\{\s+/\{/g;
$tail =~ s/\s+}/}/g;

# remove whitespaces at beginning and ending of braces
$tail =~ s/\(\s+/\(/g;
$tail =~ s/\s+\\\}/\\\}/g;

# no linebreak for scribal signs after list in ppl/ptl.
# in this case we pretend that the closing abbreviation is part of the list
$tail =~ s/\\stopitemize\\margin(.+?)\{plClose}\{d1e(.+?)}\{\\tfx\\high\{c}}\{c}/\\margin$1\{plClose}\{d1e$2}\{\\tfx\\high\{c}}\{c}\\stopitemize/g;

$tail =~ s/(\\textbackslash\}\})(\\margin\{.*?\}\{pb)/$1 $2/g;

# fixing rdgMarkers
$tail =~ s/\} \\nobreak/\}\\nobreak/g;

# fix whitespaces between rdgs and punctuation
$tail =~ s/[~\s](\\zerowidthnobreakspace)/$1/g;

#fixing whitespaces before and after scribal abbreviations
$tail =~ s/(\{\\tfx\\high\{\/[a-z]\}\}) /$1/g;
$tail =~ s/ (\{\\tfx\\high\{[a-z]\\textbackslash\}\})/$1/g;

# fixing whitespaces before punctuation
$tail =~ s/ ([,\.;\)!\?}])/$1/g;
$tail =~ s/} ([,\.;\):}])/}$1/g;
$tail =~ s/\s+(\\italic\{\.)/$1/g;
$tail =~ s/(\\stophbo)([,\.;\):}\)])/$1\\hspace\[-.2222em\]$2/g;

# fixing whitespace after punctuation
# if followed by scribal abbreviation
$tail =~ s/([,\.;\)\?}]) (\\margin\{.{8}\}\{plClose\})/$1$2/g;
$tail =~ s/(\.) (\{\\tfx\\high\{[a-z]\\textbackslash\}\})/$1$2/g;

#e2 80 9c: utf8 codepoint for LEFT DOUBLE QUOTATION MARK
$tail =~ s/ \x{201C}/\x{201C}/g;
$tail =~ s/ \xe2\x80\x9c/\xe2\x80\x9c/g;

$tail =~ s/ \./\./g;
$tail =~ s/\( \\/\(\\/g;
$tail =~ s/} \\italic\{,/}\\italic\{,/g;
$tail =~ s/} \\italic\{:/}\\italic\{:/g;
$tail =~ s/\\tfx\\high\{\/a}} \{\\tfx\\high\{\/c}/\\tfx\\high\{\/a}}\{\\tfx\\high\{\/c}/g;
$tail =~ s/(\{\\tfx\\high\{[a-z]\\textbackslash\}\}) (\\margin\{.{0,8}\}\{omClose)/$1$2/g;
$tail =~ s/\{\/a} \{\\tfx\\high\{\/c}/\{\/a}\{\\tfx\\high\{\/c}/g;
$tail =~ s/\\margindata\[inouter\]\{\/a} \{\\tfx\\high\{\/c}}/\\margindata\[inouter\]\{\/a}\{\\tfx\\high\{\/c}}/g;
# “ " « ʺ ˝ ˮ “ ̎
$tail =~ s/\s\x{201C}/\x{201C}/g;
$tail =~ s/ \x{0022}/\x{0022}/g;
$tail =~ s/ \x{00AB}/\x{00AB}/g;
$tail =~ s/ \x{02BA}/\x{02BA}/g;
$tail =~ s/ \x{02DD}/\x{02DD}/g;
$tail =~ s/ \x{02EE}/\x{02EE}/g;
$tail =~ s/ \x{201C}/\x{201C}/g;
$tail =~ s/ \x{030E}/\x{059E}/g;


# fixing §.... to §. ...
$tail =~ s/\.\.\.\./\. \.\.\./g;

# remove vertical whitespace before headings, might need further improvement
for (my $i=0; $i <= 9; $i++) {
  $tail =~ s/\\blank\[.{0,10}?\](.{0,50}?\\startsubject)/$1/g;
  $tail =~ s/\\noindent(.{0,50}?\\startsubject)/$1/g;
}

# do not break before "–"
$tail =~ s/ –/~–/g;
# break after "–" in year/page ranges
$tail =~ s/(\d)–(\d)/$1–\\hskip0pt\{\}$2/g;



# for each footnote in the critical apparatus...
# @TODO hängt sich bei Leß auf -- while terminiert nicht. Warum?
#while ($tail =~ /\\[a-z]{1,8}Note/) {
#  my $move = $tail;

#  $move =~ s/(.*?\\[a-z]{0,8}Note).*/$1/;

#  $tail = substr($tail, length($move));
#  $head = $head . $move;

#  $move = $tail;

#  $move =~ s/(.*?}).*/$1/;

#  my $count1 = ($move =~ tr/{//);
#  my $count2 = ($move =~ tr/}//);

#  my $regex = ".*?}";

#  while ($count1 != $count2) {
#    $move = $tail;

#    $regex = $regex . ".*?}";
#    $move =~ s/($regex).*/$1/;

#    $count1 = ($move =~ tr/{//);
 #   $count2 = ($move =~ tr/}//);
#  }

#  $tail = substr($tail, length($move));
#  $head = $head . $move;


  # ... find its end and add a non-breaking space if it is followed by a margin
  # or a high command

#  if ($tail =~ /^\\margin/ || $tail =~ /^{\\tfx\\high/) {
#    $head = $head . "~";
#  }
#}

$head = $head . $tail;
print encode('UTF-8', $head);
