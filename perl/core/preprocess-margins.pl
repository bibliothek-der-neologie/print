#!/usr/bin/env perl

=begin comment
Copyright (c) 2015 Hannes Riebl
Copyright (c) 2015–2019 Michelle Weidling
Copyright (c) 2020 Stefan Hynek

This file is part of bdnPrint.

bdnPrint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

bdnPrint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bdnPrint.  If not, see <https://www.gnu.org/licenses/>.
=cut

use autodie;
use strict;
use utf8;
use warnings;

use File::Slurp;
use String::Random;

use Encode qw(encode decode);

my $head = "";
my $tail = decode('UTF-8', read_file("tmp/" . $ARGV[0] . "_tmp-1.tex"));
my $random = new String::Random;

my @idArray = ();

sub generateID {
  my $id = $random->randregex("[A-Za-z]{8}");

  while (grep(/^$id/, @idArray)) {
    $id = $random->randregex("[A-Za-z]{8}");
  }

  return $id;
}

while ($tail =~ /\\margin\{}/) {
  my $move = $tail;
  my $id = generateID();

  $move =~ s/(.*?\\margin)\{\}.*/$1\{$id\}/;
  #$move =~ s/(.*?\\margin)\{\}.*/$1\{\}/;

  $tail = substr($tail, length($move) - 8);
  $head = $head . $move;
}

$head = $head . $tail;
print encode('UTF-8', $head);
