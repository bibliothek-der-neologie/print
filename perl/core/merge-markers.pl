#!/usr/bin/env perl

=begin comment
Copyright (c) 2015 Hannes Riebl
Copyright (c) 2015–2019 Michelle Weidling
Copyright (c) 2020 Stefan Hynek

This file is part of bdnPrint.

bdnPrint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

bdnPrint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bdnPrint.  If not, see <https://www.gnu.org/licenses/>.
=cut

# DEPRECATED

use autodie;
use strict;
use utf8;
use warnings;

use File::Slurp;
use List::MoreUtils qw(uniq);

use Encode qw(encode decode);

my $tmp1 = decode('UTF-8', read_file("tmp/" . $ARGV[0] . "_tmp-1.tex"));

while ($tmp1 =~ /(\\high\{\/[a-i]{0,8}\}[^~ ]*\\high\{\/[a-i]{0,8}\})/g) {
  my $search = $1;
  my $replace = $1;

  my $wits1 = $1;
  my $wits2 = $1;

  $wits1 =~ s/.*?\\high\{\/([a-i]{0,8})\}.*/$1/;
  $wits2 =~ s/.*\\high\{\/([a-i]{0,8})\}.*?/$1/;

  my $wits = $wits1 . $wits2;
  my @wits = split("", $wits);
  @wits = sort(uniq(@wits));
  $wits = join("", @wits);

  $replace =~ s/\\high\{\/[a-i]{0,8}\}/\\high\{\/$wits\}/;
  $replace =~ s/(.*)\\high\{\/[a-i]{0,8}\}/$1\\hbox\{\}/;

  $search = quotemeta($search);
  $tmp1 =~ s/$search/$replace/;
}

while ($tmp1 =~ /(\\high\{[a-i]{0,8}\\textbackslash\}[^~ ]*\\high\{[a-i]{0,8}\\textbackslash\})/g) {
  my $search = $1;
  my $replace = $1;

  my $wits1 = $1;
  my $wits2 = $1;

  $wits1 =~ s/.*?\\high\{([a-i]{0,8})\\textbackslash\}.*/$1/;
  $wits2 =~ s/.*\\high\{([a-i]{0,8})\\textbackslash\}.*?/$1/;

  my $wits = $wits1 . $wits2;
  my @wits = split("", $wits);
  @wits = sort(uniq(@wits));
  $wits = join("", @wits);

  $replace =~ s/\\high\{[a-i]{0,8}\\textbackslash\}/\\high\{$wits\\textbackslash\}/;
  $replace =~ s/(.*)\\high\{[a-i]{0,8}\\textbackslash\}/$1\\hbox\{\}/;

  $search = quotemeta($search);
  $tmp1 =~ s/$search/$replace/;
}

print encode('UTF-8', $tmp1);
