#!/usr/bin/env perl

=begin comment
Copyright (c) 2015 Hannes Riebl
Copyright (c) 2015–2019 Michelle Weidling
Copyright (c) 2020 Stefan Hynek

This file is part of bdnPrint.

bdnPrint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

bdnPrint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bdnPrint.  If not, see <https://www.gnu.org/licenses/>.
=cut

# This script evaluates which printing (e.g. 'b') and which combinations of
# printings (e.g. 'bdz') occur in a text. This information is then used to
# define the footnote classes needed for the textual apparatus in the footer of
# the page.

use autodie;
use strict;
use utf8;
use warnings;

use List::MoreUtils qw(uniq);

open(FILE, "<input/" . $ARGV[0] .".xml") or die "$!\n";

my @lines = <FILE>;
close(FILE);

my @apps;

# search lines for attribute "wit"
for my $line (@lines) {
  my @lineApps = $line =~ /wit="(.*?)"/g; # capture witness between '"' non-greedy
  s/[^a-z]//g for (@lineApps); # strip everything that is not a letter, e.g. white spaces
  push(@apps, @lineApps); # push the apparatus-id to the array "apps"
}

@apps = sort(uniq(@apps)); # strip all duplicates

my $counter = 0;

for my $app (@apps) {
  $counter = $counter + 1;

  # ConTeXt supports up to ~120 custom notes

  if ($counter <= 120) {
    print "
    \\def\\my${app}#1{%
      \\tfx\\high{${app}#1}%
    }

    \\definenote[${app}Note]
      [textcommand=\\my$app,
      paragraph=yes,
      rule=on,
      rulethickness=0px,%
    ]
    \\setupnotation[${app}Note]
    [ alternative=serried,%kein(?) hspace
      before=\{${app}},%
      numbercommand=\\tf,%
      way=bysection,%do not use bypage, duplicate variant numbers on same page possible
      width=broad,%
    ]
    "
  }
}
