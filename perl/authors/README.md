This directory contains scripts that are specific for a BdN work.
`compile.sh` expects every author to have two files, following the naming convention ${AUTHORNAME}-preprocessing.pl and  ${AUTHORNAME}-postprocessing.pl, so make sure both are available.
