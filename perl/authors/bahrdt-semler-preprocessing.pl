#!/usr/bin/env perl

=begin comment
Copyright (c) 2015 Hannes Riebl
Copyright (c) 2015–2019 Michelle Weidling
Copyright (c) 2020 Stefan Hynek

This file is part of bdnPrint.

bdnPrint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

bdnPrint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bdnPrint.  If not, see <https://www.gnu.org/licenses/>.
=cut

use autodie;
use strict;
use utf8;
use warnings;

use File::Slurp;

use Encode qw(encode decode);

my $head = "";

my $tail = decode('UTF-8', read_file("tmp/" . $ARGV[0] . "_tmp-1.tex"));

# Bring TOC into right order
#$tail =~ s/\\writetolist\[part\]\{\}\{(.*?)\}/\\writetolist\[test\]\{\}\{$1\}/g;

$tail =~ s/\\writebetweenlist\[part\]\{\{\\startalignment\[center\]\\subject\[.*?\]\{(.*?)\}\\stopalignment\}\}/\\writetolist\[part\]\{\}\{$1\}/g;
$tail =~ s/\\writetolist\[part\]\{\}\{(Bahrdt\/Semler, Glaubensbekenntnisse)\}/\\writebetweenlist\[part\]\{\{\\startalignment\[center\]\\subject\[$1]\{$1\}\\stopalignment\}\}/g;
$tail =~ s/(\\writetolist\[)chapter(\]\{\}\{Zusatz)/$1part$2/g;

# TODO [Weimarer Ausgabe].\crlf -> vskip von einer zeile (achtung. hier nicht 12pt sondern 8pt!?)
$tail =~ s/(\[Weimarer Ausgabe\]\.\\crlf)/$1\\blank\[big\]/g;
$tail =~ s/-technologischen/-tech\\-no\\-lo\\-gi\\-schen/g;
$tail =~ s/b19f\.29\.38\.57\.63/b19f.29.38.57. 63/g;

# Bahrdt/Semler has no "Teilbände", but separate texts
# $tail =~ s/Zu Teilband/Zu /g;



$head = $head . $tail;
print encode('UTF-8', $head);
