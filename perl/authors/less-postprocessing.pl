#!/usr/bin/env perl

=begin comment
Copyright (c) 2015 Hannes Riebl
Copyright (c) 2015–2019 Michelle Weidling
Copyright (c) 2020–2021 Stefan Hynek
Copyright (c) 2021 Simon Sendler

This file is part of bdnPrint.

bdnPrint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

bdnPrint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bdnPrint.  If not, see <https://www.gnu.org/licenses/>.
=cut

use autodie;
use strict;
use utf8;
use warnings;

use File::Slurp;

my $tmp = read_file("tmp/" . $ARGV[0] . "_tmp-3.tex");

#fix margins
#margin entries
$tmp =~ s/(\\margindata\[inouter\]\{\w\d{1,3}\*?([,;] \w\d{1,3}\*?)?)\}\\margindata\[inouter\]\{(\w\d{1,3}\*?([,;] \w\d{1,3}\*?)?\})/$1, $3/g;
$tmp =~ s/(\\margindata\[inouter\]\{b258, c273)(\})/$1; a274$2/g;
$tmp =~ s/\\margindata\[inouter\]\{a274\}//g;

##missing bold brackets
$tmp =~ s/(\\margindata\[inouter\]\{\/z, a!29!, b26, c26)\}(\\margin\{.*?\}\{pb\}\{\}\{\\vl\}\{a!29!\})/$1\\bb\{\}\}$2 /g;
$tmp =~ s/(\\margindata\[inouter\]\{\/z, a!45!, b42, c42)/$1\\bb\{\}/g;
$tmp =~ s/(\\margindata\[inouter\]\{\/z, a!61!, b60, c60)/$1\\bb\{\}/g;
$tmp =~ s/(\\margindata\[inouter\]\{\/z, a!79!, b80, c80)/$1\\bb\{\}/g;
$tmp =~ s/(\\margindata\[inouter\]\{\/z, a!95!, b96, c96)/$1\\bb\{\}/g;
$tmp =~ s/(\\margindata\[inouter\]\{a380; b324\[!\], c372)/$1\\bb\{\}/g;
$tmp =~ s/(\\margindata\[inouter\]\{a504, b5484, c534)/$1\\bb\{\}/g;
$tmp =~ s/(\\margindata\[inouter\]\{a667\[!\], b648, c665)/$1\\bb\{\}/g;
$tmp =~ s/(\\margindata\[inouter\]\{a668\[!\], b649, c666)/$1\\bb\{\}/g;
$tmp =~ s/(\\margindata\[inouter\]\{a676\[!\], b655, c672)/$1\\bb\{\}/g;

#fix scribal abbreviations
$tmp =~ s/(fruchtbahr werden!\})/$1\\hspace\[postSlanted\]/g;
$tmp =~ s/(Gesellschafter im Himmel!\})/$1\\hspace\[postSlanted\]/g;
$tmp =~ s/(nden vergeben!\})/$1\\hspace\[postSlanted\]/g;
$tmp =~ s/(ltigen!\})/$1\\hspace\[postSlanted\]/g;
$tmp =~ s/(der bleibt in Ewigkeit!\})/$1\\hspace\[postSlanted\]/g;
$tmp =~ s/(als Alles!\})/$1\\hspace\[postSlanted\]/g;
#doesn't work yet
$tmp =~ s/(\\subject!!\{\\italic\{Apostel-Geschicht\})/$1\\hspace\[postSlanted\]/g;

#missing white spaces
$tmp =~ s/(Christenthums sehr verst.*?rkt:)(\\margin)/$1 $2/g;
$tmp =~ s/(c573\})(\\\{\\italic\{Zweite\})/$1 $2/g;

#scribal abbreviations missing in margins
$tmp =~ s/(1781\.\\margin\{.*?\}\{plClose\}\{.{10,50}\}\{\\tfx\\high\{c\}\}\{c\})/$1\\margindata\[inouter\]\{c\}/g;
$tmp =~ s/(\\stopbodymatter)/\\margindata\[inouter\]\{bcz\\textbackslash\}$1/g;

#rotate work overview in editorial foreword
$tmp =~ s/(\\starttabulatehead\\FL \\NC \{\\switchtobodyfont\[10pt\]1\. Auflage.*?\\stoptabulate )/\\startplacetable\[location=\{force,90\},number=no\]$1\\stopplacetable /g;
$tmp =~ s/(ze\}\\NC \\AR \\LL \\stoptabulatehead\\starttabulate)\[\|p\|p\|p\|p\|\]/$1\[\|p\(4cm\)\|p\(4cm\)\|p\(4cm\)\|p\(4cm\)\|\]/g;
#$tmp =~ s/(\[Nachwort\]\\NC.*? \\NR \\BL \\stoptabulate)/$1\\stopplacetable /g;

#some referencing macros are surrounded by whitespaces, leading to visibly larger whitespaces in the output
$tmp =~ s/(\s\\\w+?Index\{[^\}]+\})\s/$1/g;
$tmp =~ s/(\s\\pagereference\[[^\]]+\])\s/$1/g;

#add page break before last paragraph of 21st sunday to avoid incorrect references
$tmp =~ s/(\\italic\{\\Spaced\{Ueberhaupt\}\} aber, lasset uns,)/\\page$1/g;

print $tmp;
