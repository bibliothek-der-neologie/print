#!/usr/bin/env perl

=begin comment
Copyright (c) 2015 Hannes Riebl
Copyright (c) 2015–2019 Michelle Weidling
Copyright (c) 2020 Stefan Hynek

This file is part of bdnPrint.

bdnPrint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

bdnPrint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bdnPrint.  If not, see <https://www.gnu.org/licenses/>.
=cut

use autodie;
use strict;
use utf8;
use warnings;

use File::Slurp;

my $tmp = read_file("tmp/" . $ARGV[0] . "_tmp-3.tex");

# in cases of \rightaligned, data in margins isn't displayed (fixed by following regexes)
$tmp =~ s/(\\rightaligned\{D\. J\. J\. Griesbach\.\\margin\{.{1,9}\}\{omClose\}\{N1\.4\.4\.2\.2\.2\.5\.6\.4\}\{\\tfx\\high\{abd\\textbackslash\}\}\{abd\\textbackslash\}\})/$1\\margindata\[inouter\]\{abd\\textbackslash\}/g;
$tmp =~ s/(J\. J\. Griesbach\.\\margin\{.{1,9}\}\{plClose\}\{N1\.4\.4\.2\.2\.2\.5\.8\.2\.1\.14\.16\.4\}\{\\tfx\\high\{b\}\}\{b\}~\\margin\{.{1,9}\}\{omClose\}\{N1\.4\.4\.2\.2\.2\.5\.8\.4\}\{\\tfx\\high\{ad\\textbackslash\}\}\{ad\\textbackslash\}\})/$1\\margindata\[inouter\]\{b, ad\\textbackslash\}/g;


$tmp =~ s/(\\margin\{.{0,8}\}\{plOpen\}\{N1\.4\.4\.2\.2\.2\.5\.8\.2\.1\.14\.16\.4\}\{\\tfx\\high\{b\}\}\{b\})\\margindata\[inouter\]\{b\}(\\personsIndex\{Griesbach, Johann Jakob\}\\crlf \\rightaligned\{)/$2$1/g;

# fixing broken margins
$tmp =~ s/(c50; d51, \/a\\textbackslash, \/a, b35)/$1\\bb\{\} a\\textbackslash, c51, d52/g;
$tmp =~ s/\\margindata\[inouter\]\{a\\textbackslash, c51, d52\}//g;
$tmp =~ s/d1371/d131/;
$tmp =~ s/d1374/d134/;
$tmp =~ s/d1379/d139/;
$tmp =~ s/(b104),( d139)/$1;$2/g;
$tmp =~ s/(d52\*),( c51\*)/$1;$2/g;
$tmp =~ s/(c10\*);( d10\*)/$1,$2/g;
$tmp =~ s/(c50\*);( d51\*)/$1,$2/g;

$tmp =~ s/(\\margindata\[inouter\]\{a100, \/a\\textbackslash, c205,) \/a/$1/g;
$tmp =~ s/(\\margindata\[inouter\]\{)(a\\textbackslash\} Gebrauche der ihm)/$1\/a\\bb\{\} $2/g;


# fix whitespace problems
$tmp =~ s/(\\dNote\{las\{\\vl\}d119\*\{\\vl\}) (sen\})/$1$2/g;
#unbi| blische
$tmp =~ s/(\\margindata\[inouter\]\{c68\*\}) (blische)/$1$2/;
#müs| sen
$tmp =~ s/(\{\\hbox\{\}\}\{d113\*\}) (sen)/$1$2/g;
#an| gebohrene
$tmp =~ s/(an\{\\vl\}b111\*\{\\vl\})[\s]+(gebohrne)/$1$2/g;
#wer| den
$tmp =~ s/(\\margindata\[inouter\]\{c110\*\}) (den)/$1$2/g;
#Begnadigung|
$tmp =~ s/(gelangt er auch zur \\italic\{Begnadigung\})/$1 /g;

# fix broken heading...
$tmp =~ s/(\\notTOCsection!!\{Zur Beschaffenheit der Originaltexte\})/\\setuphead\[notTOCsection\]\[before=\{\}\]$1/g;
# ... and reset normal settings for the next one
$tmp =~ s/(\\notTOCsection!!\{Graphematik und Interpunktion)/\\setuphead\[notTOCsection\]\[before=\{\\blank\[24pt\]\}\]$1/g;

print $tmp;