#!/usr/bin/env perl

=begin comment
Copyright (c) 2015 Hannes Riebl
Copyright (c) 2015–2019 Michelle Weidling
Copyright (c) 2020–2021 Stefan Hynek
Copyright (c) 2021 Simon Sendler

This file is part of bdnPrint.

bdnPrint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

bdnPrint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bdnPrint.  If not, see <https://www.gnu.org/licenses/>.
=cut

use autodie;
use strict;
use utf8;
use warnings;

use File::Slurp;

my $tmp = read_file("tmp/" . $ARGV[0] . "_tmp-3.tex");

# fix title pages
$tmp =~ s/(\.)(\\stopalignment\})( \\margin\{[^\}]+\}\{pb\}\{\}\{\\vl\}\{[^\}]+\}\\margindata\[inouter\]\{[^\}]+\}\{[^\}]+\}\})/$1$3$2/g;
$tmp =~ s/(\\margindata\[inouter\]\{a\[2\]\})(\\stopalignment\})(\{\\tfx\\high\{a\}\})/$1$3$2/g;

# fixes for listses
$tmp =~ s/ (\\margin\{[^\}]+\}\{pb\}\{\}\{\\vl\}\{d90\})/$1/g;
$tmp =~ s/(\\startListLike\{10\.\})/$1~/g;
$tmp =~ s/(\\startListLike\{11\.\})/$1~/g;
$tmp =~ s/ (In Absicht der Juden)/~$1/g;
$tmp =~ s/ (In Absicht des \\)/~$1/g;
$tmp =~ s/(\\startListLike\{\{\\tfx\\high\{\/a\}\}1\.\}) /$1~/g;
$tmp =~ s/(\\startListLike\{\{\\tfx\\high\{a\}\}~1\.\}) /$1~/g;

# somehow, Context does not set userpage 1 recto
$tmp =~ s/(startbodymatter)/$1 \\page\[empty\]/g;

$tmp =~ s/(doch nicht sind\.) /$1\\zerowidthnobreakspace/g;
$tmp =~ s/(\\startListLike)(\{III\.\}~In Absicht des)/$1Ppl$2/g;

print $tmp;
