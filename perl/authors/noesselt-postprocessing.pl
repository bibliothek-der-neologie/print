# A#!/usr/bin/env perl

=begin comment
Copyright (c) 2015 Hannes Riebl
Copyright (c) 2015–2019 Michelle Weidling
Copyright (c) 2020 Stefan Hynek

This file is part of bdnPrint.

bdnPrint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

bdnPrint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bdnPrint.  If not, see <https://www.gnu.org/licenses/>.
=cut

use autodie;
use strict;
use utf8;
use warnings;

use File::Slurp;

my $tmp = read_file("tmp/" . $ARGV[0] . "_tmp-3.tex");

# in cases of \rightaligned, data in margins isn't displayed (fixed by following regexes)

# A. d. H.^c
$tmp =~ s/(\\rightaligned\{[A\.\s]{0,3}[dD]{1}\. H\.\\margin\{.{0,8}\}\{plClose\}\{.{10,50}\}\{\\tfx\\high\{c\}\}\{c\}\})/$1\\margindata\[inouter\]\{c\}/g;

# A. d. H.[}]^c
$tmp =~ s/(\\rightaligned\{A\. d\. H\.\{\[\\\}\]\}\\margin\{.{0,8}\}\{plClose\}\{.{10,50}\}\{\\tfx\\high\{c\}\}\{c\}\})/$1\\margindata\[inouter\]\{c\}/g;

# A. d. H.^(c a\)
$tmp =~ s/(\\rightaligned\{A\. d\. H\.\\margin\{.{0,8}\}\{plClose\}\{.{10,50}\}\{\\tfx\\high\{c\}\}\{c\}\~)(\\margin\{.{0,8}\}\{omClose\}\{.{10,50}\}){0,1}(\{\\tfx\\high\{a\\textbackslash\}\})(\{a\\textbackslash\}\}){0,1}/$1$2$3$4\\margindata\[inouter\]\{c, a\\textbackslash\}/g;

$tmp =~ s/(\\rightaligned\{A\. d\. H\.\\\}\{\\tfx\\high\{c\}\}\~\\margin\{.{0,8}\}\{omClose\}\{.{0,50}\}\{\\tfx\\high\{a\\textbackslash\}\}\{a\\textbackslash\}\})/$1\\margindata\[inouter\]\{c, a\\textbackslash\}/g;


# A. d. H.}^c
$tmp =~ s/(\\rightaligned\{[A\.\s]{0,3}[dD]{1}\. H\.\\\}\\margin\{.{0,8}\}\{plClose\}\{.{10,50}\}\{\\tfx\\high\{c\}\}\{c\}\})/$1\\margindata\[inouter\]\{c\}/g;

# single cases of A. d. H.}^c a\
$tmp =~ s/(ndern\. \\crlf \\rightaligned\{A\. d\. H\.\\margin\{.{0,8}\}\{plClose\}\{.{0,50}\}\{\\tfx\\high\{c\}\}\{c\}\~\{\\tfx\\high\{a\\textbackslash\}\}\\margindata\[inouter\]\{c)\}\}/$1, a\\textbackslash\}\}/g;

$tmp =~ s/(sicht nehmen\. \\crlf \\rightaligned\{A\. d\. H\.\\margin\{.{0,8}\}\{plClose\}\{.{0,50}\}\{\\tfx\\high\{c\}\}\{c\}\~\{\\tfx\\high\{a\\textbackslash\}\})\}/$1\\margindata\[inouter\]\{c\}\}/g;



# cases in which a siglum is displayed in the margin but shouldn't
$tmp =~ s/(\\rightaligned\{A\. d\. H\.\\margin\{.{0,8}\}\{plClose\}\{.{10,50}\}\{\\tfx\\high\{c\}\}\{c\}\~\{\\tfx\\high\{a\\textbackslash\}\}\\margindata\[inouter\]\{c), a\\textbackslash\}\}/$1\}\}/g;

#$tmp =~ s/(\\rightaligned\{D\. H\.\\margin\{.{0,8}\}\{plClose\}\{.{10,50}\}\{\\tfx\\high\{c\}\}\{c\}\})/$1\\margindata\[inouter\]\{c\}/g;
$tmp =~ s/(\\rightaligned\{\\italic\{Der Herausgeber\.\}\\margin\{.{0,8}\}\{plClose\}\{.{10,50}\}\{\\tfx\\high\{c\}\}\{c\}\})/$1\\margindata\[inouter\]\{c\}/g;
$tmp =~ s/(\\rightaligned\{Der Herausgeber\.\\margin\{.{0,8}\}\{plClose\}\{.{10,50}\}\{\\tfx\\high\{c\}\}\{c\}\})/$1\\margindata\[inouter\]\{c\}/g;
$tmp =~ s/(\\rightaligned\{\\classicsIndex\{Cicero\}\\italic\{Cic\.\}\\margin\{.{0,8}\}\{plClose\}\{.{10,50}\}\{\\tfx\\high\{c\}\}\{c\}\})/$1\\margindata\[inouter\]\{c\}/g;

# margin overflow
$tmp =~ s/(c!3!)(\} Zweyter)/$1\]$2/g;

# wrong separation of pagebreaks
$tmp =~ s/(a413),( b128, E)/$1;$2/g;
$tmp =~ s/(c130),( b148)/$1;$2/g;
$tmp =~ s/(a117),( b143)/$1;$2/g;
$tmp =~ s/(a410\[!\]),( b142)/$1;$2/g;
$tmp =~ s/(a411\[!\]),( b143)/$1;$2/g;
$tmp =~ s/(a414\[!\]),( b146)/$1;$2/g;
$tmp =~ s/(b189),( c164)/$1;$2/g;
$tmp =~ s/(a748),( c156)/$1;$2/g;
$tmp =~ s/(\{b120),( c107)/$1;$2/g;

# bold angular bracket
$tmp =~ s/\](\} Zweyter)/\\bb\{\}$1/g;


# hyphenation
$tmp =~ s/(\{\/c\\textbackslash\}der)(selben)/$1-\\hskip0pt $2/g;
# aufhören zu sündigen
# ce b8: utf-8 codepoint for theta
# ce b5: utf-8 codepoint for epsilon
$tmp =~ s/(\xce\xb8\xce\xb5\\cNote\{)/-\\hskip0pt $1/g;
$tmp =~ s/(\\aNote\{inter)(e.?irt\})/$1-$2/g;

# editorial comments that cause empty lines
$tmp =~ s/(\\margin\{.{0,8}\}\{e\}\{\}\{\\hbox\{\}\}\{E\}\\pagereference\[.{0,8}\])(\\personsIndex\{Adelung, Johann Christoph\}\\italic\{Adelungs\}\\cNote\{\\italic\{Adelung's\}\} Magazin)/$2$1\\margindata\[inouter\]\{E\}/g;
$tmp =~ s/(So \\margin\{.{0,8}\}\{e\}\{\}\{\\hbox\{\}\}\{E\})(\\pagereference\[.{0,8}\])/$1\\margindata\[inouter\]\{E\}$2/g;
$tmp =~ s/(\\margin\{.{0,8}\}\{e\}\{\}\{\\hbox\{\}\}\{E\}\\pagereference\[.{0,8}\])(Abhandlung)/$2$1\\margindata\[inouter\]\{E\}/g;
$tmp =~ s/(\\subject!!\{10\.\})(\\noindentation )(\\margin\{.{0,8}\}\{e\}\{\}\{\\hbox\{\}\}\{E\}\\pagereference\[.{0,8}\])(.+?Aber)/$1$2$4$3/g;
$tmp =~ s/(N1\.4\.4\.2\.4\.4\.6\.22\.7\.7\.4\}\{\\tfx\\high\{\/a\}\}\{\/a\}\\margindata\[inouter\]\{)/$1E, /g;

# startrdgbeforehead instead of startrdg
$tmp =~ s/(259\\aNote\{261\}\.\}\\noindentation )\\startrdg(.*?)\\stoprdg/$1\\startrdgbeforehead$2\\stoprdgbeforehead /g;

# prevent linebreak
$tmp =~ s/(werden auch hier als apokryph eingestuft)/$1\\hskip0pt\\crlf /g;
$tmp =~ s/(und )(-gestalt der)/$1\\hskip0pt\\crlf $2/g;

# enforce linebreak
$tmp =~ s/(1547\),)/ $1/g;

# Hebrew and punctuation
$tmp =~ s/(, und dieses Letztre)/\\hspace\[interMarker\]$1/g;

# splitting editorial corrigenda
#$tmp =~ s/(c82 \\NC Sprachehren\\NC Sprachlehren\\NC \\NR \\NC)/$1\\stoptabulate\\page\\setuptabulate\[bodyfont=8\.5pt, interlinespace=20pt, rulethickness=0\.5pt, split=yes, interlinespace=10\.5pt, header=repeat, before=\{\\blank\[0pt,force\]\}, after=\{\\blank\[0pt,force\]\}\] \\starttabulatehead\\FL \\NC Seite \\NC fehlerhaftes Original \\NC stillschweigende Korrektur \\NC \\AR \\LL \\stoptabulatehead\\starttabulate\[|p\(1cm\)|p|p|\]\\NC/g;


# single cases of spacing problems
$tmp =~ s/(kerrecht\}\.\{\\tfx\\high\{c\}\})(\\stoprdg )(\\noindentation \\startauthornote .*? \\stopauthornote \\noindentation)/$1$3$2/g;


# single cases of spacing problems
$tmp =~ s/(kerrecht\}\.\{\\tfx\\high\{c\}\})(\\stoprdg )(\\noindentation \\startauthornote )(\\crlf )(.*? \\stopauthornote \\noindentation)/$1$3$5$2/g;


# fix editorial corrigenda
# Teilband 1
$tmp =~ s/c!II!( \\NC veilleicht)/cX$1/g;
$tmp =~ s/b!XVI!( \\NC 4\.)/b\[XVII\]$1/g;
$tmp =~ s/b!XVI!( \\NC 2\.)/b\[XIX\]$1/g;
$tmp =~ s/b!XVI!( \\NC die die)/b\[XIX\]$1/g;
$tmp =~ s/c214( \\NC )/c215$1/g;
$tmp =~ s/c248( \\NC )/c249$1/g;


# Teilband 3
$tmp =~ s/c135( \\NC \\italic\{Greling\})/c137$1/g;
$tmp =~ s/cIV\[!\]( \\NC Eigeues)/cX$1/g;
$tmp =~ s/c227( \\NC \\italic\{Meiner's\})/c228$1/g;

print $tmp;
