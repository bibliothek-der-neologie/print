#!/usr/bin/env perl

=begin comment
Copyright (c) 2015 Hannes Riebl
Copyright (c) 2015–2019 Michelle Weidling
Copyright (c) 2020 Stefan Hynek

This file is part of bdnPrint.

bdnPrint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

bdnPrint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bdnPrint.  If not, see <https://www.gnu.org/licenses/>.
=cut

use autodie;
use strict;
use utf8;
use warnings;

use File::Slurp;

my $head = "";

my $tail = read_file("tmp/" . $ARGV[0] . "_tmp-1.tex");

$tail =~ s/(b\[II\]\}) (\{\\tfx\\high\{ac)/$1$2/g;


# new pages to avoid orphan and widow lines
#$tail =~ s/(\\notTOCsection\[\]\{Editorische Korrekturen\})/\\page $1/g;
$tail =~ s/nisse eben so gut als die/-\\page\\noindentation nisse eben so gut als die/g;
$tail =~ s/(An drei Stellen im Textkorpus)/\\page $1/g;
#$tail =~ s/(Anmerkungen sowie Anmerkungszeichen)/\\page $1/g;
$tail =~ s/(was in ihnen ist gar nicht,)/$1\\page\\noindentation /g;
$tail =~ s/(Ansichten des Verfassers des Werks zusammen\.)/$1\\page /g;
$tail =~ s/(Uebersicht der Religionen der wichtigsten V.*?lker findet man)/$1\\page\\noindentation /g;
$tail =~ s/(Wie sie sollten getrieben werden, 280.*?287\.\\stopitemize \\stopitemize)/$1\\page\\noindentation /g;
#$tail =~ s/(ersetzender Einschub vermerkt\.)\\crlf/$1\\page\\noindentation /g;
#$tail =~ s/(Entwurf der folgenden Abhandlung .+?\. 52\.\\stopitemize)/$1\\page\\noindentation /g;
$tail =~ s/(er hat viel dazu beigetragen, den Blick)/$1\\page\\noindentation /g;
#$tail =~ s/(Geschichte 208\. 209\.\\stopitemize) (\\sym\{\}\\italic\{Vierter Abschnitt: Symbolische Theologie)/$1\\page$2/g;
#$tail =~ s/(eine von ihm wiederum)/$1\\page\\noindentation /g;
#$tail =~ s/(eigenen Uebung darin zu thun sey 60.*?67\.\\stopitemize \\stopitemize)/$1\\page\\noindentation /g;
#$tail =~ s/(\\NC Dritter Abschnitt\\NC Dritter Abschnitt\\NC Dritter Abschnitt\\NC \\NR \\NC Vierter Abschnitt)/\\stoptabulate\\page\\starttabulatehead\\FL \\NC \{\\switchtobodyfont\[10pt\]1\. Auflage\}\\NC \{\\switchtobodyfont\[10pt\]2\. Auflage \(Leittext\)\}\\NC \{\\switchtobodyfont\[10pt\]3\. Auflage\}\\NC \\AR \\LL \\stoptabulatehead\\starttabulate\[|p|p|p|\] $1/g;
#$tail =~ s/(\\notTOCsection\[\]\{Editorische Korrekturen\})/\\page $1/g;

$tail =~ s/\\newOddPage (\\marking\[oddHeader\]\{I\. Innhalt des ganzen Buchs\})/\\newPage$1/g;
$tail =~ s/\\newOddPage (\\writetolist\[section\]\{\}\{Erster Abschnitt\. \\italic\{Homiletik)/\\page\[empty\]\\noheaderandfooterlines$1/g;

#$tail =~ s/(der Verlags)(orte)/$1-\\hskip0pt\\page $2/g;
$tail =~ s/(der Verlags)(orte)/$1-\\page\\noindentation $2/g;

# in order to have an empty column heading
$tail =~ s/(\\startrdg) (\\newOddPage)/$2$1/g;

# correct top margin (has to be set manually since heading of the first preface is part of a reading)
$tail =~ s/(\\startrdg\\starteffect\[hidden\]\. \\stopeffect)\\blank\[26pt\]/$1\\blank\[30pt\]/g;
$tail =~ s/(\\blank)\[36pt(\]\\noindentation \\marking\[oddHeader\]\{Vorreden\})/$1\[30pt$2/g;

# missing paragraph
$tail =~ s/(ausgewiesen\.)(Einzelne)/$1\\crlf\\starteffect\[hidden\]\.\\stopeffect\\hspace\[p\]$2/g;

# footnote on wrong site
$tail =~ s/(erstmals literarisch als Neo)/$1-\\page\\noindentation /g;

# display rotunda
# ea 9d 9b: utf-8 codepoint for rotunda
$tail =~ s/(\xea\x9d\x9b)/\{\\freeserif \\char"A75B\}/g;

# single whitespace problems
$tail =~ s/\\hspace\[insert\]\{\\dvl\}(\\aNote\{S\.\})/$1/g;
$tail =~ s/(lkerrecht\}\.\{\\tfx\\high\{c\}\}\\stoprdg \\noindentation \\startauthornote )\\crlf \\starteffect\[hidden\]\. \\stopeffect\\hspace\[p\]\\crlf (\{\\dvl\}\\cNote\{\\italic\{Anm\.\})/$1$2/g;

# single spacing problems
$tail =~ s/(christlichen Lehre seyn\.)(\\startauthornote)/$1\\crlf $2/g;
$tail =~ s/(\\startdivsection \\subject\[\]\{22\\aNote\{524)/\\blank\[8pt\]$1/g;
$tail =~ s/(\\startdivsection \\subject\[\]\{23\\aNote\{525)/\\blank\[8pt\]$1/g;
$tail =~ s/(\\startdivsection \\subject\[\]\{24\\aNote\{526)/\\blank\[8pt\]$1/g;
$tail =~ s/(nne\.)(\\startauthornote \\hspace\[insert\])/$1\\blank\[8pt\]$2/g;

# fix headings and "Schmutztitel"
$tail =~ s/(\\marking\[oddHeader\]\{I\. \[Einleitung\]\})\\chapterhead\[\]/$1\\subject\[\]/g;
$tail =~ s/(\\marking\[oddHeader\]\{II\. \[Einleitung\]\})\\chapterhead\[\]/$1\\subject\[\]/g;
$tail =~ s/(\\marking\[oddHeader\]\{III\. \[Einleitung\]\})\\chapterhead\[\]/$1\\subject\[\]/g;

$head = $head . $tail;
print $head;
