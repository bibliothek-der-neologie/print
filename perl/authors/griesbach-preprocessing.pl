#!/usr/bin/env perl

=begin comment
Copyright (c) 2015 Hannes Riebl
Copyright (c) 2015–2019 Michelle Weidling
Copyright (c) 2020 Stefan Hynek

This file is part of bdnPrint.

bdnPrint is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

bdnPrint is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bdnPrint.  If not, see <https://www.gnu.org/licenses/>.
=cut

use autodie;
use strict;
use utf8;
use warnings;

use File::Slurp;

my $head = "";

my $tail = read_file("tmp/" . $ARGV[0] . "_tmp-1.tex");

$tail =~ s/\\startrdg \\newOddPage\\noindentation/\\page\\noheaderandfooterlines\\startrdg/g;
$tail =~ s/(\\startrdg )(\\newOddPage)/$2$1/g;

# correct even and odd pages
$tail =~ s/\\page\[empty\](\\noheaderandfooterlines\\setuppagenumber\[number=1\])/\\page$1/g;

# necessary to get right structure in TOC
$tail =~ s/\\writetolist\[section/\\writetolist\[part/g;
$tail =~ s/\\writetolist\[chapter\]\{\}\{Vorreden/\\writetolist\[part\]\{\}\{Vorreden/g;


# first two prefaces on one page plus right margin between two prefaces
$tail =~ s/\\page (\\marking\[oddHeader\]\{Vorrede zur dritten Ausgabe)/\\blank\[24pt\]$1/g;


# two title pages on a page, bigger space between title pages
$tail =~ s/(Hellers Schriften\\crlf 1779\..*?\\stoprdg)/$1 \\page/g;
$tail =~ s/(\\stopalignment\})(\\startrdg\{\\startalignment\[center\])/$1 \\blank\[36pt\]$2/g;
$tail =~ s/(\\stopalignment\}\\stoprdg) \\noindentation (\\startrdg\{\\startalignment\[center\])/$1 \\blank\[36pt\]$2/g;


# always begin on right-hand side
$tail =~ s/\\newOddPage(\\writetolist\[part\]\{\}\{V\. Zustand)/\\newPage$1/g;


# paragraphs
$tail =~ s/\\par (Anhand der vorliegenden)/\\crlf \\starteffect\[hidden\].\\stopeffect\\hspace\[p\]$1/g;


# special hyphenation cases
# anfäng|-lich
$tail =~ s/(ng\}) (\\margin)/$1$2/g;
$tail =~ s/ (\\italic\{lich in)/$1/g;
# c.f. https://tex.stackexchange.com/questions/35370/allowing-breaks-at-a-location-with-no-spaces-or-hyphens?rq=1
$tail =~ s/(\{\/a\}Betrach)(tungen)/$1-\\hskip0pt $2/g;
$tail =~ s/(vorzuneh)(men\{\\tfx)/$1-\\hskip0pt $2/g;
$tail =~ s/(Schriften, 1787)/\\hskip0pt $1/g;


# editorial corrigenda

$tail =~ s/(a5 \\NC nnd\\NC und\\NC \\NR \\NC)/$1\\stoptabulate \\setuptabulate\[bodyfont=8\.5pt, interlinespace=20pt, rulethickness=0\.5pt, split=yes, interlinespace=10\.5pt, header=repeat, before=\{\\blank\[0pt,force\]\}, after=\{\\blank\[0pt,force\]\}\] \\starttabulatehead\\FL \\NC Seite \\NC fehlerhaftes Original \\NC stillschweigende Korrektur \\NC \\AR \\LL \\stoptabulatehead\\starttabulate\[|p\(1cm\)|p|p|\]\\NC/g;
#$tail =~ s/(Gesinnungen\\NC \\NR \\HL \\stoptabulate)/$1 \\setuptabulate\[bodyfont=8\.5pt, interlinespace=20pt, rulethickness=0\.5pt, split=yes, interlinespace=10\.5pt, header=repeat, before=\{\\blank\[12pt,force\]\}, after=\{\\blank\[12pt,force\]\}\]/g;

# special cases of pagebreaks
$tail =~ s/(dagogischen Vor)/$1-\\page\\noindentation /g;
$tail =~ s/(wird erst im Zuge)/$1\\page\\noindentation /g;
$tail =~ s/(rezipiert, sondern aktiv und wirkungsvoll mitbestimmt hat, wird erst im Zuge)/\\startalignment\[paragraph\]$1\\stopalignment/g;




$head = $head . $tail;
print $head;