## 4.2.0
### Added
- white space rule for punctuation
- separate heading style for Leß's Bible references in headings
- TOC for Bahrdt/Semler
### Changed
- lots of docs
### Fixed
- white space and margin problems in Leß

## 4.1.0 - 2019-02-27
### Added
### Changed
### Fixed
- (white) space problems in Nösselt
- page number issue (right and left side have been switched)


## 4.0.0 - 2019-02-25
### Added
- The whole compilation process now runs in a Docker contained
### Changed
- Updated README to be compliant with Docker setting


## 3.1.0 - 2019-02-20
### Added
- In case a work has several parts (teilbaende), each part has its own table for editorial corrigenda

### Changed
- Enhanced hyphenation rules
- Switches page numbers (now odd ones are on the right, even ones the left)

### Fixed
- Fixed bug in margin markers
- Fixed sorting of entries in subjects index
- Fixed bug in typearea
- Fixed bug in bidi when Hebrew word/character is followed by punctuation
- Fixes bugs in whitespace setting
- White space problems in Leß

## 3.0.0 - 2019-02-04
- Added everything that is relevant for Griesbach.

## 2.2.0 - 2018-01-19
### Added
- Overfull margins are split so that margin texts no longer overlap.

## 2.1.0. - 2018-01-18
### Added
- When two or more pagebreaks in margin text refer to the same pagebreak marker in textarea, they are now seperated by a comma.
- When two or more pagebreaks in margin text DON'T refer to the same pagebreak marker in textarea, they are now seperated by a semicolon.

### Changed
- More realistic classification of Semantic Versioning for this project.


## 0.0.1 - 2018-01-16
### Added
- This CHANGELOG file.
