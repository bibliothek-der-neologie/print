FROM ubuntu:20.04 as builder

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update; \
    apt-get install -yy \
    wget \
    unzip

RUN mkdir -p /saxon; \
    wget https://repo1.maven.org/maven2/net/sf/saxon/Saxon-HE/9.9.1-8/Saxon-HE-9.9.1-8.jar -O /saxon/saxon9he.jar; \
    wget https://repo1.maven.org/maven2/net/sf/saxon/Saxon-HE/10.5/Saxon-HE-10.5.jar -O /saxon/saxon10he.jar

RUN mkdir -p /context; \
    wget http://lmtx.pragma-ade.nl/install-lmtx/context-linux-64.zip -O /tmp/context-linux-64.zip; \
    unzip /tmp/context-linux-64.zip -d /context; \
    cd /context; \
    sh install.sh

RUN mkdir -p /fonts; \
    wget https://github.com/octaviopardo/EBGaramond12/archive/refs/heads/master.zip -O /tmp/ebgaramond.zip; \
    wget https://software.sil.org/downloads/r/ezra/EzraSIL-2.51.zip -O /tmp/ezrazil.zip; \
    unzip /tmp/ebgaramond.zip -d /fonts; \
    unzip /tmp/ezrazil.zip -d /fonts


FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update; \
    apt-get install -yy \
        # java
        openjdk-8-jdk \
        # perl + modules
        perl \
        libterm-readline-perl-perl \
        libfile-slurp-perl \
        libautodie-perl \
        libstring-random-perl \
        liblist-moreutils-perl \
        libmodule-install-perl \
        libhttp-server-simple-perl \
        # luatex
        texlive-luatex \
        # fonts
        fonts-ebgaramond \
        fonts-ebgaramond-extra \
        fonts-linuxlibertine \
        # pdf
        poppler-utils \
        pdftk \
        # `get` script needs curl
        curl \
        # helper
        nano \
        locate


ARG UID=${UID}
ARG GID=${GID}

RUN groupadd --gid ${GID} ${UID}; \
    useradd --uid ${UID} --gid ${GID} --create-home ${UID}


COPY --from=builder /context /usr/local/context
COPY --from=builder --chown=${UID}:${GID} /saxon /opt/saxon
# Copy fonts into the system fonts directories
COPY --from=builder /fonts/EBGaramond12-master/fonts/otf/ /usr/share/fonts/opentype/ebgaramond/
COPY --from=builder /fonts/EzraSIL2.51/*.ttf /usr/share/fonts/truetype/ezrazil/

ENV OSFONTDIR /usr/share/fonts:/usr/local/share/fonts
ENV TEXROOT /usr/local/context/tex
ENV PATH /usr/local/context/tex/texmf-linux-64/bin:/usr/local/context/bin:$PATH

# Refresh font db
RUN mtxrun --script fonts --reload; \
    luaotfload-tool --update

WORKDIR /app

USER ${UID}:${GID}

CMD ["bash"]
