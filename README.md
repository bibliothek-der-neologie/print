# bdnPrint

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

> Generate printable PDFs for the "Bibliothek der Neologie".

bdnPrint is a collection of stylesheets and scripts to generate printable PDFs
from TEI/XML. It was originally developed for the DFG-funded
[Bibliothek der Neologie](http://www.bdn-edition.de) project and runs on an
[intermediate format](https://gitlab.gwdg.de/bibliothek-der-neologie/intermediate-format)
that is based on the project's TEI/XML schema.

## Prerequisites

* Docker
* Fonts (see below)

### Fonts

The font used in bdnPrint is [**EB Garamond**](http://www.georgduffner.at/ebgaramond/), which is a free Garamond typeface.
For Hebrew words, we use [**Ezra SIL**](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=EzraSIL_Home).
To use these fonts, create a folder `fonts` in this repo's root directory and extract the fonts there in separate folders (`EBGaramond-0.016` and `EzraSIL2.51`).
You don't have to worry about installing them; Docker takes care of that.

(Download:
https://bitbucket.org/georgd/eb-garamond/downloads/EBGaramond-0.016.zip
https://software.sil.org/downloads/r/ezra/EzraSIL-2.51.zip
)

Of course you can use any other font you wish. Simply change the line

    \setmainfont[ebgaramond]

in `context/header.tex` to an already installed font.

## Installing

After cloning the repository, there are two ways to install the app: via the
provided convenience scripts (recommended), or manually.

### Convenience Installation

bdnPrint provides convenience scripts for building and running the container,
respectively.

First, call

    build

This script calls docker-compose with all the necessary variables. Then, call

    run

When this is finished, the container should be up and running.

### Manual Installation

Note: the manual installation also works with podman/buildah.

Then, run

    docker build -t $(name) .

This takes some time since we create a complete Ubuntu 16.04 environment with
all software required for compiling.

As soon as this is finished, run

    docker run -ti -v "$PWD":/app $(name)

which gives you a shell inside the container.

## Use

Inside the container, call

    compile $(author)

for a single author's work, or call

    compile all

to create a PDF for each available XML file in `input/`.
Since the Docker container is synced with the repo's directory on your file
system, you also have the PDF available locally.

## Known issues

For known issues please refer to the [issue board](https://gitlab.gwdg.de/bibliothek-der-neologie/print/issues).

## Authors

* **Hannes Riebl** - *2015* - [hriebl](https://github.com/hriebl)
* **Michelle Weidling** - *2015-2019* - [mrodzis](https://gitlab.gwdg.de/mrodzis)
* **Stefan Hynek** - *2020-2021* - [hynek](https://gitlab.gwdg.de/hynek)
* **Simon Sendler** - *2021* - [sendler](https://gitlab.gwdg.de/sendler)

See also the list of [contributors](https://github.com/subugoe/bdnPrint/contributors)
who participated in this project.
